#pragma once

#define PLATFORM_IS_MACOSX() defined(__APPLE__)

#if defined(_WIN32)
#define PLATFORM_IS_WINDOWS() (_WIN32)
#else
#define PLATFORM_IS_WINDOWS() 0
#endif

namespace Platform
{
    // ----------------------------------------------------------------------------------------------------
    // Platform ID Utilities
    
    enum class PlatformID
    {
        Windows,
        MacOSX
    };
    
    constexpr PlatformID GetNativePlatform() noexcept
    {
#if PLATFORM_IS_MACOSX()
        return PlatformID::MacOSX;
#elif PLATFORM_IS_WINDOWS()
        return PlatformID::Windows;
#else
#error Target platform unknown, plese implement
#endif
    }
    
    constexpr bool IsNativePlatform( PlatformID platform )
    {
        return platform == GetNativePlatform();
    }
    
    // ----------------------------------------------------------------------------------------------------
    // Endianness Utilities
    
    enum class Endianness
    {
        LittleEndian,
        BigEndian
    };
    
    constexpr Endianness operator!( Endianness endianness ) noexcept
    {
        switch( endianness )
        {
            case Endianness::LittleEndian:
            {
                return Endianness::BigEndian;
            } break;
            case Endianness::BigEndian:
            {
                return Endianness::LittleEndian;
            } break;
        }
    }
    
    constexpr Endianness GetEndiannessByPlatform( PlatformID platform ) noexcept
    {
        switch( platform )
        {
            case PlatformID::Windows:
            {
                return Endianness::LittleEndian;
            } break;
            case PlatformID::MacOSX:
            {
                return Endianness::LittleEndian;
            } break;
        }
    }
    
    constexpr Endianness GetNativeEndianness() noexcept
    {
        return GetEndiannessByPlatform( GetNativePlatform() );
    }
}
