#pragma once

#include <cassert>
#include <cstring>

namespace MemUtils
{
    // Implemented with standard memcpy, but strictly accepts std::byte type for destination and source
    inline void Memcpy( std::byte* pDest, const std::byte* pSource, std::size_t numBytes ) noexcept
    {
        std::memcpy( pDest, pSource, numBytes);
    }

    // Implemented with standard memcpy, but copies the data from a trivial type to another trivial type, both passed by-reference
    template < typename DestType, typename SourceType >
    inline void Memcpy( DestType& rDest, const SourceType& rSource ) noexcept
    {
        static_assert( std::is_trivial_v< DestType >, "Destination type must be trivial" );
        static_assert( std::is_trivial_v< SourceType >, "Source type must be trivial" );
        static_assert( sizeof( DestType ) == sizeof( SourceType ), "Destination must be the same size as the target" );
        std::memcpy( &rDest, &rSource, sizeof( SourceType ) );
    }

    // Implemented with standard memcpy, but copies the data from a trivial type and returns by-value to another trivial type
    template < typename DestType, typename SourceType >
    [[nodiscard]]
    inline DestType MemReadAs( const SourceType& rSource ) noexcept
    {
        DestType result;
        Memcpy( result, rSource );
        return result;
    }

    // Slower than memcpy, explicitly copies data to destination from source in reverse byte order
    inline void ReverseMemcpy( std::byte* pDest, const std::byte* pSource, std::size_t numBytes ) noexcept
    {
        assert( pDest );
        assert( pSource );
        const std::size_t lastIndex = numBytes - 1;
        for ( std::size_t byteIndex = 0; byteIndex < numBytes; ++byteIndex )
        {
            const std::size_t byteIndexInverse = lastIndex - byteIndex;
            pDest[byteIndex] = pSource[byteIndexInverse];
        }
    }

    // Implemented with standard memset, strictly operates on std::byte-passed data
    inline void MemZero( std::byte* pData, std::size_t Size ) noexcept
    {
        assert( pData );
        std::memset( pData, 0x00, Size );
    }

    // Implemented with standard memset, zeroes an instance of a trivial type
    template < typename ZeroedType >
    inline void MemZero( ZeroedType& rData ) noexcept
    {
        static_assert( std::is_trivial_v< ZeroedType >, "Type must be trivial" );
        std::memset( &rData, 0x00, sizeof( ZeroedType ) );
    }

    // Implemented with standard memset, zeroes a raw array of a trivial type
    template < typename ZeroedType, std::size_t ItemCount >
    inline void MemZero( ZeroedType( &Arr )[ItemCount] ) noexcept
    {
        static_assert( std::is_trivial_v< ZeroedType >, "Type must be trivial" );
        constexpr std::size_t ZeroedSize = sizeof( ZeroedType ) * ItemCount;
        std::memset( &Arr, 0x00, ZeroedSize );
    }
}
