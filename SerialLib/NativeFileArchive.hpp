#pragma once

#include <cstddef>
#include <cstdint>

#include "ArchiveTraits.hpp"

class NativeFileArchive final
{
public:
    enum class ExistenceCondition
    {
        MustExistOnOpen,
        CreateOrOverwriteOnOpen
    };
    
	[[nodiscard]]
    static bool Open( NativeFileArchive& fileArchive, const char* filename, ExistenceCondition existCond );
    void Close();
    bool IsOpen() const noexcept;

    NativeFileArchive() noexcept;
    ~NativeFileArchive();
    
    // Write an instance or an array of instances by-pointer
    template < typename DataT >
    inline void Write( const DataT* pSourceData, std::size_t count )
    {
        static_assert( std::is_trivially_copyable_v<DataT>, "DataT instances must be trivially copyable." );
        const std::size_t fullWriteSize = sizeof( DataT ) * count;
        WriteImpl( reinterpret_cast<const std::byte*>( pSourceData ), fullWriteSize );
    }
    
    // Write a single instance (accepts rvalue and literal without needing named temporaries)
    template < typename DataT >
    inline void Write( const DataT rSourceData )
    {
        Write( &rSourceData, 1 );
    }
    
    // Read an instance or an array of instances by-pointer
    template < typename DataT >
    inline void Read( DataT* pDestData, std::size_t count ) const
    {
        static_assert( std::is_trivially_copyable_v<DataT>, "DataT instances must be trivially copyable." );
        const std::size_t fullReadSize = sizeof( DataT ) * count;
        ReadImpl( reinterpret_cast<std::byte*>(pDestData), fullReadSize );
    }
    
    // Read a single instance and return it by-value; NVRO may and should elide the copy
    template < typename DataT >
	[[nodiscard]]
    inline const DataT Read() const
    {
        DataT result;
        Read( &result, 1 );
        return result;
    }
    
    std::size_t GetSeek() const;
    void SetSeek( std::size_t newSeek ) const;
    void SeekToEnd() const;
    
    std::size_t GetSize() const;
    
private:
    NativeFileArchive( const NativeFileArchive& ) = delete;
    NativeFileArchive& operator=( const NativeFileArchive& ) = delete;
    
    void WriteImpl( const std::byte* pSourceData, std::size_t count );
    void ReadImpl( std::byte* pDestData, std::size_t count ) const;
    
    using HandleAlias = std::size_t;
    static const HandleAlias DefaultNullFileHandle;
    HandleAlias handle;
};

// Override default ArchiveTraits
namespace ArchiveTraits
{
    template <>
    struct IsArchive_Trait<NativeFileArchive> final
    {
        static constexpr bool value = true;
    };
    
    template < typename DataType >
    struct TriviallySerializesType_Trait<NativeFileArchive, DataType> final
    {
        static constexpr bool value = std::is_trivially_copyable_v<DataType>;
    };
    
    template <>
    struct SafeToSerializeFromNativeSystem_Trait<NativeFileArchive> final
    {
        static constexpr bool value = true;
    };
}
