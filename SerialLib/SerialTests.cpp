#include "SerialTests.hpp"

#include <limits>

#include "TestUtils.hpp"

#include "ArrayUtils.hpp"
#include "NativeBufferArchive.hpp"
#include "NativeFileArchive.hpp"
#include "NativeDummySizeArchive.hpp"
#include "SerialInterface.hpp"
#include "SerialSTLContainers.hpp"

struct HasTransient final
{
    static constexpr int32_t TransientDefaultValue = 0;
    
    int32_t a = 0;
    int32_t someTransient = TransientDefaultValue;
    int32_t b = 0;
};

namespace SerialTraits
{
    // Override this trait for HasTransient so it can't be trivially serialized
    template <>
    struct TypeMayBeTriviallySerialized_Trait<HasTransient> final
    {
        static constexpr bool value = false;
    };
}

template < typename ArchiveType, typename SerialOpT >
inline void SerialImpl( SerialOpT, ArchiveType& rArchive, SerialInterface::AmbidextrousArg<SerialOpT, HasTransient> data )
{
    using namespace SerialInterface;
    SerialImpl( SerialOpT{}, rArchive, data.a );
    // Don't serialize someTransient
    SerialImpl( SerialOpT{}, rArchive, data.b );
}

struct HasVirtuals
{
    HasVirtuals() = default;
    virtual ~HasVirtuals() = default;
    
    std::uint32_t DoSomethingWithMyNumber()
    {
        return DoSomethingWithMyNumberImpl();
    }
    
    std::uint32_t myNum = 0;
protected:
    virtual std::uint32_t DoSomethingWithMyNumberImpl()
    {
        myNum *= 2;
        return myNum;
    }
};

template < typename ArchiveType, typename SerialOpT >
inline void SerialImpl( SerialOpT, ArchiveType& rArchive, SerialInterface::AmbidextrousArg<SerialOpT, HasVirtuals> data )
{
    using namespace SerialInterface;
    SerialImpl( SerialOpT{}, rArchive, data.myNum );
}

struct DerivedVirtual final
    : public HasVirtuals
{
    DerivedVirtual() = default;
    virtual ~DerivedVirtual() override = default;
    
    std::uint32_t addToNum = 0;
private:
    virtual std::uint32_t DoSomethingWithMyNumberImpl() override
    {
        myNum *= myNum;
        myNum += addToNum;
        return myNum;
    }
};

template < typename ArchiveType, typename SerialOpT >
inline void SerialImpl( SerialOpT, ArchiveType& rArchive, SerialInterface::AmbidextrousArg<SerialOpT, DerivedVirtual> data )
{
    using namespace SerialInterface;
    SerialImpl( SerialOpT{}, rArchive, static_cast<AmbidextrousArg<SerialOpT, HasVirtuals>>( data ) );
    SerialImpl( SerialOpT{}, rArchive, data.addToNum );
}

struct TriviallyCopyableStruct final
{
    bool bWillTriviallySerialize = false;
};

class ClassWithPrivateString final
{
public:
    template < typename SourceStringType >
	ClassWithPrivateString( SourceStringType&& sourceStringValue )
        : myPrivString{ std::forward<SourceStringType>( sourceStringValue ) }
    {
    }

	ClassWithPrivateString() = default;
	~ClassWithPrivateString() = default;
	ClassWithPrivateString( const ClassWithPrivateString& ) = delete;
	ClassWithPrivateString& operator=( const ClassWithPrivateString& ) = delete;

	const std::string_view GetMyString() const
	{
		return myPrivString;
	}

    template < typename SetToType >
	void SetMyString( SetToType&& setToValue )
	{
		myPrivString = std::forward<SetToType>( setToValue );
	}

private:
	std::string myPrivString{ "" };
};

template < typename ArchiveType, typename SerialOpT >
inline void SerialImpl( SerialOpT, ArchiveType& rArchive, SerialInterface::AmbidextrousArg<SerialOpT, ClassWithPrivateString&> rStringyClass )
{
    using namespace SerialInterface;
    
    if constexpr ( IsStoring<SerialOpT> )
    {
        // Do this if we're storing
        SerialStore( rArchive, rStringyClass.GetMyString() );
    }
    else if ( IsLoading<SerialOpT> )
    {
        // Do this if we're loading
        rStringyClass.SetMyString( SerialLoad<std::string>( rArchive ) );
    }
}

namespace SerialTests
{
    static TestReturn TestNativeBufferArchiveBasic()
    {
        TEST_START( "TestNativeBufferArchiveBasic" );
        
        // Import the SerialInterface namespace
        using namespace SerialInterface;
        
        // Individual values
        {
            // Serialize to the buffer archive
            const NativeBufferArchive readArch = []
            {
                // Store
                NativeBufferArchive writeArch;
                SerialStore<uint32_t>( writeArch, 12345 );
                SerialStore<float>( writeArch, 3.14f );
                SerialStore<double>( writeArch, 4.51 );
                SerialStore( writeArch, TriviallyCopyableStruct{ true } );

                // Reset seek and return archive for reading
                writeArch.SetSeek( 0 );
                return writeArch;
            }();
            
            // Check the appropriate number of bytes have been written to the archive
            CHECK( readArch.GetSize() == 17 );
            
            // Deserialize from the buffer archive and check results
            {
                CHECK( readArch.GetSeek() == 0 );
                
                // Load
                const uint32_t num32_result = SerialLoad<uint32_t>( readArch );
                const float flt_result = SerialLoad<float>( readArch );
                
                double dbl_result;
                SerialLoad( readArch, &dbl_result );
                
                const TriviallyCopyableStruct trivialStruct = SerialLoad<TriviallyCopyableStruct>( readArch );
                
                // Checks
                constexpr uint32_t num32_check = 12345;
                constexpr float flt_check = 3.14f;
                constexpr double dbl_check = 4.51;
                
                CHECK( num32_result == num32_check );
                CHECK( flt_result == flt_check );
                CHECK( dbl_result == dbl_check );
                CHECK( trivialStruct.bWillTriviallySerialize == true );
            }
        }
        
        // Values in an array
        {
            // Serialize to the buffer archive
            const NativeBufferArchive readArch = []
            {
                constexpr float numbers[3] = { 1.23f, 4.56f, 7.89f };

                // Store
                NativeBufferArchive writeArch;
                SerialStore<float>( writeArch, numbers, ArrayUtils::CountOf( numbers ) );
                
                // Reset seek and return archive for reading
                writeArch.SetSeek( 0 );
                return writeArch;
            }();
            
            // Check the appropriate number of bytes have been written to the archive
            CHECK( readArch.GetSize() == 12 );
            
            // Deserialize from the buffer archive and check results
            {
                CHECK( readArch.GetSeek() == 0 );
                
                // Load
                float numbers_results[3];
                SerialLoad<float>( readArch, numbers_results, ArrayUtils::CountOf( numbers_results ) );
                
                // Checks
                CHECK( numbers_results[0] == 1.23f );
                CHECK( numbers_results[1] == 4.56f );
                CHECK( numbers_results[2] == 7.89f );
            }
        }
        
        TEST_END_PRINT();
    }
    
    static TestReturn TestNativeFileArchiveBasic()
    {
        TEST_START( "TestNativeFileArchiveBasic" );
        
        // Import the SerialInterface namespace
        using namespace SerialInterface;
        
        // Individual values
        {
            // Serialize to the file archive
            constexpr char archiveFilename[] = ".\\TestFile.abc";
            {
                // Open file archive for writing
                NativeFileArchive fileWriteArch;
                const bool bSuccess = NativeFileArchive::Open( fileWriteArch, archiveFilename, NativeFileArchive::ExistenceCondition::CreateOrOverwriteOnOpen );
                assert( bSuccess );
                
                // Store
                SerialStore<uint32_t>( fileWriteArch, 12345 );
                SerialStore<float>( fileWriteArch, 3.14f );
                SerialStore<double>( fileWriteArch, 4.51 );
                SerialStore( fileWriteArch, TriviallyCopyableStruct{ true } );
            }
            
            // Deserialize from the file archive and check results
            {
                // Open file archive for reading
                NativeFileArchive fileReadArch;
                const bool bSuccess = NativeFileArchive::Open( fileReadArch, archiveFilename, NativeFileArchive::ExistenceCondition::MustExistOnOpen );
                assert( bSuccess );
                
                CHECK( fileReadArch.GetSeek() == 0 );
                
                // Load
                const uint32_t num32_result = SerialLoad<uint32_t>( fileReadArch );
                const float flt_result = SerialLoad<float>( fileReadArch );
                
                double dbl_result;
                SerialLoad( fileReadArch, &dbl_result );
                
                const TriviallyCopyableStruct trivialStruct = SerialLoad<TriviallyCopyableStruct>( fileReadArch );
                
                // Checks
                constexpr uint32_t num32_check = 12345;
                constexpr float flt_check = 3.14f;
                constexpr double dbl_check = 4.51;
                
                CHECK( num32_result == num32_check );
                CHECK( flt_result == flt_check );
                CHECK( dbl_result == dbl_check );
                CHECK( trivialStruct.bWillTriviallySerialize == true );
            }
        }
        
        // Values in an array
        {
            // Serialize to the file archive
            constexpr char archiveFilename[] = ".\\TestFile.abc";
            {
                constexpr float numbers[3] = { 1.23f, 4.56f, 7.89f };

                // Open file archive for writing
                NativeFileArchive fileWriteArch;
                const bool bSuccess = NativeFileArchive::Open( fileWriteArch, archiveFilename, NativeFileArchive::ExistenceCondition::CreateOrOverwriteOnOpen );
                assert( bSuccess );
                
                // Store
                SerialStore<float>( fileWriteArch, numbers, ArrayUtils::CountOf( numbers ) );
            }
            
            // Deserialize from the file archive and check results
            {
                // Open file archive for reading
                NativeFileArchive fileReadArch;
                const bool bSuccess = NativeFileArchive::Open( fileReadArch, archiveFilename, NativeFileArchive::ExistenceCondition::MustExistOnOpen );
                assert( bSuccess );
                
                CHECK( fileReadArch.GetSeek() == 0 );
                
                // Load
                float numbers_results[3];
                SerialLoad<float>( fileReadArch, numbers_results, ArrayUtils::CountOf( numbers_results ) );
                
                // Checks
                CHECK( numbers_results[0] == 1.23f );
                CHECK( numbers_results[1] == 4.56f );
                CHECK( numbers_results[2] == 7.89f );
            }
        }
        
        TEST_END_PRINT();
    }
    
    static TestReturn TestNonTrivialClass()
    {
        TEST_START( "TestNonTrivialClass" );
        
        // Import the SerialInterface namespace
        using namespace SerialInterface;
        
        // Serialize to the buffer archive
        const NativeBufferArchive readArch = []
        {
            HasTransient hasTransient;
            hasTransient.a = 1234;
            hasTransient.someTransient = 1337;
            hasTransient.b = 1776;

            // Store
            NativeBufferArchive writeArch;
            SerialStore( writeArch, hasTransient );
                
            // Reset seek and return archive for reading
            writeArch.SetSeek( 0 );
            return writeArch;
        }();
            
        // Check the appropriate number of bytes have been written to the archive
        CHECK( readArch.GetSize() == 8 );
            
        // Deserialize from the buffer archive and check results
        {
            CHECK( readArch.GetSeek() == 0 );
                
            // Load
            const HasTransient hasTransient_result = SerialLoad<HasTransient>( readArch );
                
            // Checks - the transient should be left as default
            CHECK( hasTransient_result.a == 1234 );
            CHECK( hasTransient_result.b == 1776 );
                
            CHECK( hasTransient_result.someTransient == HasTransient::TransientDefaultValue );
        }
        
        TEST_END_PRINT();
    }
    
    static TestReturn TestPointer()
    {
        TEST_START( "TestPointer" );
        
        // Import the SerialInterface namespace
        using namespace SerialInterface;
        
        // Serialize to the buffer archive
        const NativeBufferArchive readArch = []
        {
            uint32_t* const pU32 = new uint32_t{ 12345u };
            int* const pIntNull = nullptr;
            float* const pFlt = new float{ 3.14f };
                
            // Store
            NativeBufferArchive writeArch;
            SerialStore( writeArch, pU32 );
            SerialStore( writeArch, pIntNull );
            SerialStore( writeArch, pFlt );
                
            // Cleanup allocations
            delete pU32;
            delete pFlt;
                
            // Reset seek and return archive for reading
            writeArch.SetSeek( 0 );
            return writeArch;
        }();
            
        // Check the appropriate number of bytes have been written to the archive
        CHECK( readArch.GetSize() == 11 );
            
        // Deserialize from the buffer archive and check results
        {
            CHECK( readArch.GetSeek() == 0 );
                
            // Load
            const uint32_t* const pU32_result = SerialLoad<uint32_t*>( readArch );
            const int* const pIntNull_result = SerialLoad<int*>( readArch );
                
            float* pFlt_result = nullptr;
            SerialLoad( readArch, &pFlt_result );

            // Checks
            CHECK( pU32_result != nullptr );
            CHECK( pIntNull_result == nullptr );
            CHECK( pFlt_result != nullptr );

            CHECK( *pU32_result == 12345u );
            CHECK( *pFlt_result == 3.14f );

            // Cleanup allocations
            delete pU32_result;
            delete pFlt_result;
        }
        
        TEST_END_PRINT();
    }
    
    static TestReturn TestRawCArray()
    {
        TEST_START( "TestRawCArray" );

        // Import the SerialInterface namespace
        using namespace SerialInterface;

        // Serialize to the buffer archive
        const NativeBufferArchive readArch = []
        {
            const float fltArr[3] = { 1.23f, 4.56f, 7.89f };
            const HasTransient trnsArr[2] = { { 1, 2, 3 }, { 4, 5, 6 } };
            ClassWithPrivateString pstrArr[2];
            pstrArr[0].SetMyString( "First" );
            pstrArr[1].SetMyString( "Second" );

            // Store
            NativeBufferArchive writeArch;
            SerialStore( writeArch, fltArr );
            SerialStore( writeArch, trnsArr );
            SerialStore( writeArch, pstrArr );

            // Reset seek and return archive for reading
            writeArch.SetSeek( 0 );
            return writeArch;
        }( );

        // Deserialize from the buffer archive and check results
        {
            CHECK( readArch.GetSeek() == 0 );

            float fltArrResult[3];
            HasTransient trnsArrResult[2];
            ClassWithPrivateString pstrArrResult[2];

            // Load
            SerialLoad( readArch, &fltArrResult );
            SerialLoad( readArch, &trnsArrResult );
            SerialLoad( readArch, &pstrArrResult );

            // Checks
            CHECK( fltArrResult[0] == 1.23f );
            CHECK( fltArrResult[1] == 4.56f );
            CHECK( fltArrResult[2] == 7.89f );

            CHECK( trnsArrResult[0].a == 1 );
            CHECK( trnsArrResult[0].someTransient == HasTransient::TransientDefaultValue );
            CHECK( trnsArrResult[0].b == 3 );
            CHECK( trnsArrResult[1].a == 4 );
            CHECK( trnsArrResult[1].someTransient == HasTransient::TransientDefaultValue );
            CHECK( trnsArrResult[1].b == 6 );

            CHECK( pstrArrResult[0].GetMyString() == "First" );
            CHECK( pstrArrResult[1].GetMyString() == "Second" );
        }

        TEST_END_PRINT();
    }

    static TestReturn TestStdArray()
    {
        TEST_START( "TestStdArray" );
        
        // Import the SerialInterface namespace
        using namespace SerialInterface;
        
        // Serialize to the buffer archive
        const NativeBufferArchive readArch = []
        {
            const std::array<int, 5> arrA = { 1, 1, 2, 3, 5 };
            const std::array<float, 3> arrB = { 1.23f, 4.56f, 7.89f };
            const std::array<HasTransient, 2> arrC =
            {
                HasTransient{ 0, 1, 2 },
                HasTransient{ 3, 4, 5 }
            };
            const std::array<ClassWithPrivateString, 2> arrD =
            {
                ClassWithPrivateString{ "First" },
                ClassWithPrivateString{ "Second" }
            };
            
            // Store
            NativeBufferArchive writeArch;
            SerialStore( writeArch, arrA );
            SerialStore( writeArch, arrB );
            SerialStore( writeArch, arrC );
            SerialStore( writeArch, arrD );
            
            // Reset seek and return archive for reading
            writeArch.SetSeek( 0 );
            return writeArch;
        }();
        
        // Deserialize from the buffer archive and check results
        {
            CHECK( readArch.GetSeek() == 0 );
            
            // Load
            const std::array<int, 5> arrAResult = SerialLoad<std::array<int, 5>>( readArch );
            const std::array<float, 3> arrBResult = SerialLoad<std::array<float, 3>>( readArch );
            const std::array<HasTransient, 2> arrCResult = SerialLoad<std::array<HasTransient, 2>>( readArch );
            
            std::array<ClassWithPrivateString, 2> arrDResult;
            SerialLoad( readArch, &arrDResult );
            
            // Check
            CHECK( arrAResult[0] == 1 );
            CHECK( arrAResult[1] == 1 );
            CHECK( arrAResult[2] == 2 );
            CHECK( arrAResult[3] == 3 );
            CHECK( arrAResult[4] == 5 );

            CHECK( arrBResult[0] == 1.23f );
            CHECK( arrBResult[1] == 4.56f );
            CHECK( arrBResult[2] == 7.89f );

            CHECK( arrCResult[0].a == 0 );
            CHECK( arrCResult[0].someTransient == HasTransient::TransientDefaultValue );
            CHECK( arrCResult[0].b == 2 );
            CHECK( arrCResult[1].a == 3 );
            CHECK( arrCResult[1].someTransient == HasTransient::TransientDefaultValue );
            CHECK( arrCResult[1].b == 5 );
            
            CHECK( arrDResult[0].GetMyString() == "First" );
            CHECK( arrDResult[1].GetMyString() == "Second" );
        }
        
        TEST_END_PRINT();
    }
    
    static TestReturn TestStdVector()
    {
        TEST_START( "TestStdVector" );
        
        // Import the SerialInterface namespace
        using namespace SerialInterface;
        
        // Serialize to the buffer archive
        const NativeBufferArchive readArch = []
        {
            std::vector<float> numberVector;
            numberVector.emplace_back( 1.23f );
            numberVector.emplace_back( 4.56f );
            numberVector.emplace_back( 7.89f );
                
            const std::vector<uint32_t> emptyVector;
                
            // Store
            NativeBufferArchive writeArch;
            SerialStore( writeArch, numberVector );
            SerialStore( writeArch, emptyVector );

            // Reset seek and return archive for reading
            writeArch.SetSeek( 0 );
            return writeArch;
        }();

        // Check the appropriate number of bytes have been written to the archive
        CHECK( readArch.GetSize() == 28 );
            
        // Deserialize from the buffer archive and check results
        {
            CHECK( readArch.GetSeek() == 0 );
                
            // Load
            const std::vector<float> numberVector_result = SerialLoad<std::vector<float>>( readArch );
            const std::vector<uint32_t> emptyVector_result = SerialLoad<std::vector<uint32_t>>( readArch );
                
            // Checks
            CHECK( numberVector_result.size() == 3 );
            CHECK( emptyVector_result.empty() );
                
            CHECK( numberVector_result[0] == 1.23f );
            CHECK( numberVector_result[1] == 4.56f );
            CHECK( numberVector_result[2] == 7.89f );
        }
        
        TEST_END_PRINT();
    }
    
    static TestReturn TestStdList()
    {
        TEST_START( "TestStdList" );
        
        // Import the SerialInterface namespace
        using namespace SerialInterface;

        // Serialize to the buffer archive
        const NativeBufferArchive readArch = [] ()
        {
            std::list<int> listInt;
            listInt.push_front( 4 );
            listInt.push_front( 3 );
            listInt.push_front( 2 );
            listInt.push_front( 1 );
            listInt.push_front( 0 );
            
            std::list<HasTransient> listNonTrivial;
            listNonTrivial.push_back( HasTransient{ 0, 1, 2 } );
            listNonTrivial.push_back( HasTransient{ 3, 4, 5 } );
            listNonTrivial.push_back( HasTransient{ 6, 7, 8 } );
            
            // Store
            NativeBufferArchive writeArch;
            SerialStore( writeArch, listInt );
            SerialStore( writeArch, listNonTrivial );
            
            // Reset seek and return archive for reading
            writeArch.SetSeek( 0 );
            return writeArch;
        }();
        
        // Deserialize from the buffer archive and check results
        {
            // Load
            const std::list<int> listIntResult = SerialLoad<std::list<int>>( readArch );
            std::list<HasTransient> listNonTrivialResult = SerialLoad<std::list<HasTransient>>( readArch );

            // Checks - funamental int
            {
                CHECK( listIntResult.size() == 5 );

                std::list<int>::const_iterator iterInt = listIntResult.begin();
                CHECK( iterInt != listIntResult.end() && *iterInt == 0 );
                
                ++iterInt;
                CHECK( iterInt != listIntResult.end() && *iterInt == 1 );
                
                ++iterInt;
                CHECK( iterInt != listIntResult.end() && *iterInt == 2 );
                
                ++iterInt;
                CHECK( iterInt != listIntResult.end() && *iterInt == 3 );
                
                ++iterInt;
                CHECK( iterInt != listIntResult.end() && *iterInt == 4 );
            }
            
            // Checks - non-trivially-serializable type
            {
                CHECK( listNonTrivialResult.size() == 3 );

                std::list<HasTransient>::const_iterator iterNonTrivial = listNonTrivialResult.begin();
                const HasTransient nonTrivA = *iterNonTrivial;
                CHECK( nonTrivA.a == 0 );
                CHECK( nonTrivA.someTransient == HasTransient::TransientDefaultValue );
                CHECK( nonTrivA.b == 2 );
                
                ++iterNonTrivial;
                const HasTransient nonTrivB = *iterNonTrivial;
                CHECK( nonTrivB.a == 3 );
                CHECK( nonTrivB.someTransient == HasTransient::TransientDefaultValue );
                CHECK( nonTrivB.b == 5 );
                
                ++iterNonTrivial;
                const HasTransient nonTrivC = *iterNonTrivial;
                CHECK( nonTrivC.a == 6 );
                CHECK( nonTrivC.someTransient == HasTransient::TransientDefaultValue );
                CHECK( nonTrivC.b == 8 );
            }
        }
        
        TEST_END_PRINT();
    }

    static TestReturn TestStdStringView()
    {
        TEST_START( "TestStdStringView" );
        
        // Import the SerialInterface namespace
        using namespace SerialInterface;

        // Serialize to the buffer archive
        const NativeBufferArchive readArch = []
        {
            // Store after initing data
            NativeBufferArchive writeArch;
            
            {
                const std::string bigString = "There's word in this string";
                const std::string_view subStringView = std::string_view{bigString}.substr( 8, 4 );
                SerialStore( writeArch, subStringView );
            }
            
			{
				// std::vector SerialImpl is defined before string_view's in the dedicated header,
				//	so if not specified might use trivial serialization when it shouldn't
				const char* originalStrings[] = { "first", "second", "third" };
				const std::vector<std::string_view> stringViews = { originalStrings[0], originalStrings[1], originalStrings[2] };
				SerialStore( writeArch, stringViews );
			}

            {
                SerialStore<std::string_view>( writeArch, "Just a string" );
            }

			{
				const ClassWithPrivateString stringyClass{ "My private string" };
				SerialStore( writeArch, stringyClass );
			}
            
            // Reset seek and return archive for reading
            writeArch.SetSeek( 0 );
            return writeArch;
        }();

        // Deserialize from the buffer archive and check results
        {
            CHECK( readArch.GetSeek() == 0 );
            
            // Load
            const std::string subString = SerialLoad<std::string>( readArch );
            const std::vector<std::string> stringsFromStringViewStore = SerialLoad<std::vector<std::string>>( readArch );
            const std::string serializedPlainString = SerialLoad<std::string>( readArch );

            ClassWithPrivateString stringyClass;
            SerialLoad( readArch, &stringyClass );
            
            // Checks
            CHECK( subString.size() == 4 );
            CHECK( subString == "word" );
            
			CHECK( stringsFromStringViewStore[0] == "first" );
			CHECK( stringsFromStringViewStore[1] == "second" );
			CHECK( stringsFromStringViewStore[2] == "third" );

            CHECK( serializedPlainString.size() == 13 );
            CHECK( serializedPlainString == "Just a string" );

			CHECK( stringyClass.GetMyString().size() == 17 );
			CHECK( stringyClass.GetMyString() == "My private string" );
        }
        
        TEST_END_PRINT();
    }
    
    static TestReturn TestDummySizeArchive()
    {
        TEST_START( "TestDummySizeArchive" );
        
        // Import the SerialInterface namespace
        using namespace SerialInterface;
        
        // Perform some faux-serialization on a size archive, getting number of bytes which would have been written

        {
            // Pseudo-store
            NativeDummySizeArchive sizeArchive;
            SerialStore<std::byte>( sizeArchive, static_cast<std::byte>( 0x66 ) );
            SerialStore<float>( sizeArchive, 4.0f );
            
            // Check byte count of data
            CHECK( sizeArchive.GetSize() == 5 );
        }
        
        {
            struct SizeTestStruct final
            {
                double a = 0.0;
                uint32_t b = 0;
                double c = 0.0;
                float d = 0.0f;
                uint16_t e = 0;
                uint64_t f = 0;
            };
            
            const SizeTestStruct testData[3];
            
            // Pseudo-store
            NativeDummySizeArchive sizeArchive;
            SerialStore( sizeArchive, testData, ArrayUtils::CountOf( testData ) );
            SerialStore<uint32_t>( sizeArchive, 44 );
            SerialStore<double>( sizeArchive, 12.34 );
            
            // Check byte count of data
            CHECK( sizeArchive.GetSize() == 132 );
        }
        
        {
            // Pseudo-store
            NativeDummySizeArchive sizeArchive;
            SerialStore<HasTransient>( sizeArchive, HasTransient{} );

            // Check byte count of data
            CHECK( sizeArchive.GetSize() == 8 );
        }
        
        TEST_END_PRINT();
    }
    
    static TestReturn TestVirtualAndDerivedClasses()
    {
        TEST_START( "TestVirtualAndDerivedClasses" );
        
        // Import the SerialInterface namespace
        using namespace SerialInterface;
        
        // Serialize to the buffer archive
        const NativeBufferArchive readArch = []
        {
            HasVirtuals testHasVirtuals{};
            testHasVirtuals.myNum = 4;
            testHasVirtuals.DoSomethingWithMyNumber();
                
            DerivedVirtual testDerivedVirtual{};
            testDerivedVirtual.myNum = 3;
            testDerivedVirtual.addToNum = 2;
            testDerivedVirtual.DoSomethingWithMyNumber();

            // Store
            NativeBufferArchive writeArch;
            SerialStore( writeArch, testHasVirtuals );
            SerialStore( writeArch, testDerivedVirtual );
                
            // Reset seek and return archive for reading
            writeArch.SetSeek( 0 );
            return writeArch;
        }();
            
        // Check the appropriate number of bytes have been written to the archive
        CHECK( readArch.GetSize() == 12 );
            
        // Deserialize from the buffer archive and check results
        {
            CHECK( readArch.GetSeek() == 0 );
                
            // Load
            HasVirtuals testHasVirtuals_result = SerialLoad<HasVirtuals>( readArch );
            DerivedVirtual testDerivedVirtuals_result = SerialLoad<DerivedVirtual>( readArch );
                
            // Checks
            CHECK( testHasVirtuals_result.myNum == 8 );
            CHECK( testHasVirtuals_result.DoSomethingWithMyNumber() == 16 );
                
            CHECK( testDerivedVirtuals_result.myNum == 11 );
            CHECK( testDerivedVirtuals_result.addToNum == 2 );
            CHECK( testDerivedVirtuals_result.DoSomethingWithMyNumber() == 123 );
        }
        
        TEST_END_PRINT();
    }
    
    static TestReturn TestPackageWithHeader()
    {
        TEST_START( "TestPackageWithHeader" );
        
        // Import the SerialInterface namespace
        using namespace SerialInterface;
        
        struct PackageHeader final
        {
            std::uint32_t numFloats = 0;
            std::int64_t floatsOffset = -1;
            
            std::uint32_t numBytes = 0;
            std::int64_t bytesOffset = -1;
        };
        
        constexpr std::byte F = static_cast<std::byte>( 0x0 );
        constexpr std::byte T = static_cast<std::byte>( 0x1 );
        
        // Serialize the package
        const NativeBufferArchive readArch = [F, T]
        {
            NativeBufferArchive writeArch;
            
            // Stub in dummy package header
            PackageHeader packHdr{};
            SerialStore( writeArch, packHdr );
            
            // Write bytes first (tracking the offset allows us to arbitrarily read this specific section later)
            const std::byte bytes[] = { T, T, F, T, F, T };
            packHdr.numBytes = static_cast<std::uint32_t>( ArrayUtils::CountOf( bytes ) );
            packHdr.bytesOffset = writeArch.GetSeek();
            SerialStore( writeArch, bytes, packHdr.numBytes );
            
            // Write floats second (tracking the offset allows us to arbitrarily read this specific section later)
            const float floats[] = { 1.2f, 3.4f, 5.6f, 7.8f, 9.1f, 2.3f, 4.5f };
            packHdr.numFloats = static_cast<std::uint32_t>( ArrayUtils::CountOf( floats ) );
            packHdr.floatsOffset = writeArch.GetSeek();
            SerialStore( writeArch, floats, packHdr.numFloats );
            
            // Rewrite correct package header
            writeArch.SetSeek( 0 );
            SerialStore( writeArch, packHdr );
            
            // Reset seek and return archive for reading
            writeArch.SetSeek( 0 );
            return writeArch;
        }();
        
        // Deserialize the package
        {
            CHECK( readArch.GetSeek() == 0 );
            
            // Read header for import info on each section
            const PackageHeader packHdr = SerialLoad<PackageHeader>( readArch );
            CHECK( packHdr.numFloats == 7 );
            CHECK( packHdr.floatsOffset > 0 );
            CHECK( packHdr.numBytes == 6 );
            CHECK( packHdr.bytesOffset > 0 );
            
            // Reliably read all floats from their dedicated section somewhere in the package
            std::vector<float> floats( packHdr.numFloats );
            if constexpr ( sizeof( std::size_t ) < sizeof( std::int64_t ) )
			{
                assert( packHdr.floatsOffset <= static_cast<std::int64_t>( std::numeric_limits<std::size_t>::max() ) );
			}
            readArch.SetSeek( static_cast<std::size_t>( packHdr.floatsOffset ) );
            SerialLoad( readArch, floats.data(), packHdr.numFloats );
            CHECK( floats[0] == 1.2f );
            CHECK( floats[1] == 3.4f );
            CHECK( floats[2] == 5.6f );
            CHECK( floats[3] == 7.8f );
            CHECK( floats[4] == 9.1f );
            CHECK( floats[5] == 2.3f );
            CHECK( floats[6] == 4.5f );
            
            // Reliably read all bytes from their dedicated section somewhere in the package
            std::vector<std::byte> bytes( packHdr.numBytes );
			if constexpr ( sizeof( std::size_t ) < sizeof( std::int64_t ) )
			{
                assert( packHdr.bytesOffset <= static_cast<std::int64_t>( std::numeric_limits<std::size_t>::max() ) );
			}
            readArch.SetSeek( static_cast<std::size_t>( packHdr.bytesOffset ) );
            SerialLoad( readArch, bytes.data(), packHdr.numBytes );
            CHECK( bytes[0] == T );
            CHECK( bytes[1] == T );
            CHECK( bytes[2] == F );
            CHECK( bytes[3] == T );
            CHECK( bytes[4] == F );
            CHECK( bytes[5] == T );
        }
        
        TEST_END_PRINT();
    }
    
    static TestReturn TestStagingBuffer()
    {
        TEST_START( "TestStagingBuffer" );
        
        // Import the SerialInterface namespace
        using namespace SerialInterface;
        
        constexpr char Filename[] = "StagedData.poo";
        
        // Get serialized data out to a file
        {
            // Begin by serializing to a buffer archive
            NativeBufferArchive stagingBuffer;
            SerialStore<std::uint32_t>( stagingBuffer, 1776 );
            SerialStore<std::string>( stagingBuffer, std::string( "My test string" ) );
            SerialStore<float>( stagingBuffer, 3.14f );
            
            // Write the bytes from the buffer archive out to a file with one contiguous write
            NativeFileArchive fileWriter;
            const bool bSuccess = NativeFileArchive::Open( fileWriter, Filename, NativeFileArchive::ExistenceCondition::CreateOrOverwriteOnOpen );
            assert( bSuccess );
            
            fileWriter.Write( stagingBuffer.GetRawStorage(), stagingBuffer.GetSize() );
        }
        
        // Re-open the source file and read all bytes into a buffer archive with one contiguous read
        NativeBufferArchive stagingBuffer;
        {
            NativeFileArchive fileReader;
            const bool bSuccess = NativeFileArchive::Open( fileReader, Filename, NativeFileArchive::ExistenceCondition::MustExistOnOpen );
            assert( bSuccess );
                
            const std::size_t fileSize = fileReader.GetSize();
            stagingBuffer.Resize( fileSize );
            fileReader.Read( stagingBuffer.GetRawStorage(), fileSize );
        }
            
        // Deserialize data from the buffer archive
        const std::uint32_t num = SerialLoad<std::uint32_t>( stagingBuffer );
        const std::string str = SerialLoad<std::string>( stagingBuffer );
        const float flt = SerialLoad<float>( stagingBuffer );
                
        // Checks
        CHECK( num == 1776 );
        CHECK( str == "My test string" );
        CHECK( flt == 3.14f );
        
        TEST_END_PRINT();
    }
    
	static TestReturn TestArrayOfNonTrivial()
	{
		TEST_START( "TestArrayOfNonTrivial" );

        // Import the SerialInterface namespace
        using namespace SerialInterface;

        // Serialize to the buffer archive
		NativeBufferArchive arch;
		{
            // This should use non-trivial array serialization, which should use the type's custom
            //  SerialImpl()
			const HasTransient nonTrivials[] = { HasTransient{1, 2, 3}, HasTransient{ 4, 5, 6 } };
			SerialStore( arch, nonTrivials, ArrayUtils::CountOf( nonTrivials ) );

            // Reset seek and return archive for reading
            arch.SetSeek( 0 );
		}

        // Deserialize from the buffer archive and check results
        {
			CHECK( arch.GetSeek() == 0 );

            // Same as above, but for the Load operation
			HasTransient nonTrivials[2];
			SerialLoad( arch, nonTrivials, ArrayUtils::CountOf( nonTrivials ) );

            // Checks
			CHECK( nonTrivials[0].a == 1 );
			CHECK( nonTrivials[0].b == 3 );
			CHECK( nonTrivials[0].someTransient == HasTransient::TransientDefaultValue );

			CHECK( nonTrivials[1].a == 4 );
			CHECK( nonTrivials[1].b == 6 );
			CHECK( nonTrivials[1].someTransient == HasTransient::TransientDefaultValue );
		}

		TEST_END_PRINT();
	}

	static TestReturn TestArrayOfPointers()
	{
		TEST_START( "TestArrayOfPointers" );

        // Import the SerialInterface namespace
        using namespace SerialInterface;

        // Serialize to the buffer archive
        NativeBufferArchive arch;
		{
			uint32_t* pointers[3] = { nullptr, nullptr, nullptr };

			pointers[0] = new uint32_t{ 12 };
			assert( pointers[0] );

			pointers[2] = new uint32_t{ 34 };
			assert( pointers[2] );

            // This should use non-trivial array serialization, which should use the specialized
            //  SerialImpl() for pointers
            SerialStore( arch, pointers, ArrayUtils::CountOf( pointers ) );

            // Cleanup allocations
            for ( uint32_t* ptr : pointers )
            {
                delete ptr;
            }

            // Reset seek and return archive for reading
            arch.SetSeek( 0 );
		}

        // Deserialize from the buffer archive and check results
        {
            CHECK( arch.GetSeek() == 0 );

            // Same as above, but for the Load operation
            uint32_t* pointers[3] = { nullptr, nullptr, nullptr };
            SerialLoad( arch, pointers, ArrayUtils::CountOf( pointers ) );

            // Checks
            CHECK( pointers[0] != nullptr );
            CHECK( pointers[1] == nullptr );
            CHECK( pointers[2] != nullptr );

            CHECK( *pointers[0] == 12 );
            CHECK( *pointers[2] == 34 );

            // Cleanup allocations
            for ( uint32_t* ptr : pointers )
            {
                delete ptr;
            }
        }

		TEST_END_PRINT();
	}

	static TestReturn TestVectorArrayOfPointersToNonTrivial()
	{
		TEST_START( "TestVectorArrayOfPointersToNonTrivial" );

        // Import the SerialInterface namespace
        using namespace SerialInterface;

        // Serialize to the buffer archive
        const NativeBufferArchive readableArch = []
		{
			std::vector<HasTransient*> nonTrivials;
			{
                nonTrivials.resize( 7, nullptr );

				nonTrivials[0] = new HasTransient{ 1, 2, 3 };
				assert( nonTrivials[0] );

				nonTrivials[1] = new HasTransient{ 4, 5, 6 };
				assert( nonTrivials[1] );

				nonTrivials[4] = new HasTransient{ 7, 8, 9 };
				assert( nonTrivials[4] );

				nonTrivials[5] = new HasTransient{ 10, 11, 12 };
				assert( nonTrivials[5] );
			}

            // This should use custom stl vector SerialImpl() non-trivial array serialization,
            //  which should use the specialized SerialImpl() for pointers, which should use the
            //  type's custom SerialImpl()
            NativeBufferArchive writableArch;
            SerialStore( writableArch, nonTrivials );

            // Cleanup allocations
            for ( HasTransient* pDeleteMe : nonTrivials )
			{
				delete pDeleteMe;
			}

            // Reset seek and return archive for reading
            writableArch.SetSeek( 0 );
			return writableArch;
		}();

        // Deserialize from the buffer archive and check results
        {
			CHECK( readableArch.GetSeek() == 0 );

            // Same as above, but for the Load operation
            const std::vector<HasTransient*> nonTrivials = SerialLoad<std::vector<HasTransient*>>( readableArch );

            // Checks
            CHECK( nonTrivials.size() == 7 );

            CHECK( nonTrivials[0] != nullptr );
            CHECK( nonTrivials[0]->a == 1 );
            CHECK( nonTrivials[0]->b == 3 );
            CHECK( nonTrivials[0]->someTransient == HasTransient::TransientDefaultValue );

            CHECK( nonTrivials[1] != nullptr );
            CHECK( nonTrivials[1]->a == 4 );
            CHECK( nonTrivials[1]->b == 6 );
            CHECK( nonTrivials[1]->someTransient == HasTransient::TransientDefaultValue );

            CHECK( nonTrivials[2] == nullptr );
            CHECK( nonTrivials[3] == nullptr );

            CHECK( nonTrivials[4] != nullptr );
            CHECK( nonTrivials[4]->a == 7 );
            CHECK( nonTrivials[4]->b == 9 );
            CHECK( nonTrivials[4]->someTransient == HasTransient::TransientDefaultValue );

            CHECK( nonTrivials[5] != nullptr );
            CHECK( nonTrivials[5]->a == 10 );
            CHECK( nonTrivials[5]->b == 12 );
            CHECK( nonTrivials[5]->someTransient == HasTransient::TransientDefaultValue );

            CHECK( nonTrivials[6] == nullptr );

            // Cleanup allocations
            for ( HasTransient* pNonTrivial : nonTrivials )
            {
                delete pNonTrivial;
            }
		}

		TEST_END_PRINT();
	}
    
    
#if !defined( TEST_AGGREGATE )
#define TEST_AGGREGATE( out_total_tests, out_total_checks, out_tests_passing, test_function ) { TestReturn reportTmp = test_function(); ++out_total_tests; out_total_checks += reportTmp.numTests; out_tests_passing += ( ( reportTmp.failures.size() == 0 ) ? 1 : 0 ); }
#else
#error TEST_AGGREGATE is defined elsewhere
#endif
    
    void Run()
    {
        printf( "\n----------------------------------------------------------------------------------------------------\n" );
        printf( "[SerialTests]\n\n" );
        
		std::uint32_t numTotalTests = 0;
        std::uint32_t numTotalChecks = 0;
		std::uint32_t numTestsPassing = 0;

		TEST_AGGREGATE( numTotalTests, numTotalChecks, numTestsPassing, TestNativeBufferArchiveBasic );
		TEST_AGGREGATE( numTotalTests, numTotalChecks, numTestsPassing, TestNativeFileArchiveBasic );
		TEST_AGGREGATE( numTotalTests, numTotalChecks, numTestsPassing, TestNonTrivialClass );
		TEST_AGGREGATE( numTotalTests, numTotalChecks, numTestsPassing, TestPointer );
        TEST_AGGREGATE( numTotalTests, numTotalChecks, numTestsPassing, TestRawCArray );
        TEST_AGGREGATE( numTotalTests, numTotalChecks, numTestsPassing, TestStdArray );
		TEST_AGGREGATE( numTotalTests, numTotalChecks, numTestsPassing, TestStdVector );
        TEST_AGGREGATE( numTotalTests, numTotalChecks, numTestsPassing, TestStdList );
        TEST_AGGREGATE( numTotalTests, numTotalChecks, numTestsPassing, TestStdStringView );
		TEST_AGGREGATE( numTotalTests, numTotalChecks, numTestsPassing, TestDummySizeArchive );
		TEST_AGGREGATE( numTotalTests, numTotalChecks, numTestsPassing, TestVirtualAndDerivedClasses );
		TEST_AGGREGATE( numTotalTests, numTotalChecks, numTestsPassing, TestPackageWithHeader );
		TEST_AGGREGATE( numTotalTests, numTotalChecks, numTestsPassing, TestStagingBuffer );
		TEST_AGGREGATE( numTotalTests, numTotalChecks, numTestsPassing, TestArrayOfNonTrivial );
		TEST_AGGREGATE( numTotalTests, numTotalChecks, numTestsPassing, TestArrayOfPointers );
		TEST_AGGREGATE( numTotalTests, numTotalChecks, numTestsPassing, TestVectorArrayOfPointersToNonTrivial );

		printf( "Tests: %u of %u passing\n", numTestsPassing, numTotalTests );
        printf( "Total CHECKs: %u\n\n", numTotalChecks );
    }

#undef TEST_AGGREGATE

}
