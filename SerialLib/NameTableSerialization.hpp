#pragma once

#include "NameTable.hpp"
#include "SerialInterface.hpp"
#include "SerialSTLContainers.hpp"

template < typename ArchiveType >
void SerialImpl( SerialCore::SerialOp::StoreT, ArchiveType& rArchiveDest, const NameTable& rNameTableSource )
{
    using namespace SerialInterface;
    SerialStore( rArchiveDest, rNameTableSource.GetTableEntries() );
}

template < typename ArchiveType >
void SerialImpl( SerialCore::SerialOp::LoadT, const ArchiveType& rArchiveSource, NameTable& rNameTableDest )
{
    using namespace SerialInterface;
    for( const std::string& entry : SerialLoad<std::vector<std::string>>( rArchiveSource ) )
    {
        rNameTableDest.FindOrAdd( entry );
    }
}
