#pragma once

#include <cstddef>

template < typename... Types >
struct TypeList;

namespace TypeListImpl
{
    template < typename... >
    struct SizeImpl;
    
    template < typename... Types >
    struct SizeImpl<TypeList<Types...>> final
    {
        static constexpr std::size_t value = sizeof...(Types);
    };
}

namespace TypeListFuncs
{
    template < typename Seq >
    constexpr std::size_t Size = TypeListImpl::SizeImpl<Seq>::value;
}

namespace TypeListImpl
{
    template < typename... >
    struct FrontImpl;
    
    template < typename FirstType, typename... RemainingTypes >
    struct FrontImpl<TypeList<FirstType, RemainingTypes...>> final
    {
        using type = FirstType;
    };
}

namespace TypeListFuncs
{
    template < typename Seq >
    using Front = typename TypeListImpl::FrontImpl<Seq>::type;
}

namespace TypeListImpl
{
    template < typename... >
    struct PopFrontImpl;
    
    template < typename FirstType, typename... RemainingTypes >
    struct PopFrontImpl<TypeList<FirstType, RemainingTypes...>> final
    {
        using type = TypeList<RemainingTypes...>;
    };
}

namespace TypeListFuncs
{
    template < typename Seq >
    using PopFront = typename TypeListImpl::PopFrontImpl<Seq>::type;
}

namespace TypeListImpl
{
    template < typename Seq, template <typename> typename... Pragmas >
    struct SatisfiesImpl;
    
    template < template <typename> typename FirstPragma, template <typename> typename... RemainingPragmas, typename... Types >
    struct SatisfiesImpl<TypeList<Types...>, FirstPragma, RemainingPragmas...> final
    {
        static constexpr bool value
            = SatisfiesImpl<TypeListFuncs::PopFront<TypeList<Types...>>, FirstPragma>::value
            && SatisfiesImpl<TypeList<Types...>, RemainingPragmas...>::value;
    };
    
    template < template <typename> typename OnlyPragma, typename... Types >
    struct SatisfiesImpl<TypeList<Types...>, OnlyPragma> final
    {
        static constexpr bool value
            = OnlyPragma<TypeListFuncs::Front<TypeList<Types...>>>::value
            && SatisfiesImpl<TypeListFuncs::PopFront<TypeList<Types...>>, OnlyPragma>::value;
    };
    
    template < typename OnlyType, template <typename> typename FirstPragma, template <typename> typename... RemainingPragmas >
    struct SatisfiesImpl<TypeList<OnlyType>, FirstPragma, RemainingPragmas...> final
    {
        static constexpr bool value
            = FirstPragma<OnlyType>::value
            && SatisfiesImpl<TypeList<OnlyType>, RemainingPragmas...>::value;
    };
    
    template < template <typename> typename OnlyPragma, typename OnlyType >
    struct SatisfiesImpl<TypeList<OnlyType>, OnlyPragma> final
    {
        static constexpr bool value = OnlyPragma<OnlyType>::value;
    };
}

namespace TypeListFuncs
{
    template < typename Seq, template <typename > typename... Pragmas >
    constexpr bool Satisfies = TypeListImpl::SatisfiesImpl<Seq, Pragmas...>::value;
}

namespace TypeListImpl
{
    template < typename... >
    struct ContainsImpl;
    
    template < typename FindMe, typename... Types >
    struct ContainsImpl<TypeList<Types...>, FindMe> final
    {
        static constexpr bool value
            = std::is_same_v<TypeListFuncs::Front<TypeList<Types...>>, FindMe>
            || ContainsImpl<TypeListFuncs::PopFront<TypeList<Types...>>, FindMe>::value;
    };
    
    template < typename FindMe, typename OnlyType >
    struct ContainsImpl<TypeList<OnlyType>, FindMe> final
    {
        static constexpr bool value = std::is_same_v<OnlyType, FindMe>;
    };
    
    template < typename FindMe >
    struct ContainsImpl<TypeList<>, FindMe> final
    {
        static constexpr bool value = false;
    };
}

namespace TypeListFuncs
{
    template < typename Seq, typename FindMe >
    constexpr bool Contains = TypeListImpl::ContainsImpl<Seq, FindMe>::value;
}

namespace TypeListImpl
{
    template < typename... >
    struct UniqueImpl;

    template < typename... Types >
    struct UniqueImpl<TypeList<Types...>> final
    {
        static constexpr bool value
            = !TypeListFuncs::Contains<TypeListFuncs::PopFront<TypeList<Types...>>, TypeListFuncs::Front<TypeList<Types...>>>
            && UniqueImpl<TypeListFuncs::PopFront<TypeList<Types...>>>::value;
    };
    
    template < typename OnlyType >
    struct UniqueImpl<TypeList<OnlyType>> final
    {
        static constexpr bool value = true;
    };
}

namespace TypeListFuncs
{
    template < typename Seq >
    constexpr bool Unique = TypeListImpl::UniqueImpl<Seq>::value;
}

namespace TypeListImpl
{
    template < typename... >
    struct AppendImpl;
    
    template < typename... LeftTypes, typename... RightTypes, typename... AdditonalSeqs >
    struct AppendImpl<TypeList<LeftTypes...>, TypeList<RightTypes...>, AdditonalSeqs...> final
    {
        using type = typename AppendImpl<TypeList<LeftTypes..., RightTypes...>, AdditonalSeqs...>::type;
    };
    
    template < typename... LeftTypes, typename... RightTypes >
    struct AppendImpl<TypeList<LeftTypes...>, TypeList<RightTypes...>> final
    {
        using type = TypeList<LeftTypes..., RightTypes...>;
    };
}

namespace TypeListFuncs
{
    template < typename... Seqs >
    using Append = typename TypeListImpl::AppendImpl<Seqs...>::type;
}

namespace TypeListImpl
{
    template < typename... >
    struct CompareOrderedImpl;
    
    template < typename... LeftTypes, typename... RightTypes >
    struct CompareOrderedImpl<TypeList<LeftTypes...>, TypeList<RightTypes...>> final
    {
        static constexpr bool value
            = std::is_same_v< TypeListFuncs::Front<TypeList<LeftTypes...>>, TypeListFuncs::Front<TypeList<RightTypes...>>>
            && CompareOrderedImpl< TypeListFuncs::PopFront<TypeList<LeftTypes...>>, TypeListFuncs::PopFront<TypeList<RightTypes...>>>::value;
    };
    
    template < typename LeftType, typename... RightTypes >
    struct CompareOrderedImpl<TypeList<LeftType>, TypeList<RightTypes...>> final
    {
        static constexpr bool value = false;
    };
    
    template < typename RightType, typename... LeftTypes >
    struct CompareOrderedImpl<TypeList<LeftTypes...>, TypeList<RightType>> final
    {
        static constexpr bool value = false;
    };
    
    template < typename LeftType, typename RightType >
    struct CompareOrderedImpl<TypeList<LeftType>, TypeList<RightType>> final
    {
        static constexpr bool value = std::is_same_v<LeftType, RightType>;
    };
    
    template <>
    struct CompareOrderedImpl<TypeList<>, TypeList<>> final
    {
        static constexpr bool value = true;
    };
}

namespace TypeListFuncs
{
    template < typename LeftSeq, typename RightSeq >
    constexpr bool CompareOrdered = TypeListImpl::CompareOrderedImpl<LeftSeq, RightSeq>::value;
}

namespace TypeListImpl
{
    template < typename... >
    struct ConvertToTypeListImpl;
    
    template < template <typename...> typename TemplateType, typename... TypeParams >
    struct ConvertToTypeListImpl<TemplateType<TypeParams...>> final
    {
        using type = TypeList<TypeParams...>;
    };
}

namespace TypeListFuncs
{
    template < typename TemplateType >
    using ConvertToTypeList = typename TypeListImpl::ConvertToTypeListImpl<TemplateType>::type;
}
