#pragma once

#include <cstdint>
#include <string>
#include <string_view>
#include <vector>

class NameTable final
{
public:
    using NameID = std::int32_t;
    static constexpr NameID NullNameID = static_cast<NameID>( -1 );
    
    NameTable() = default;
    ~NameTable() = default;
    NameTable( const NameTable& ) = default;
    NameTable& operator=( const NameTable& ) = default;
    
    [[nodiscard]]
    NameID Find( std::string_view nameString ) const;
    NameID FindOrAdd( std::string_view nameString );
    
    [[nodiscard]]
    const std::string_view GetNameString( NameID ID ) const;

    [[nodiscard]]
    const std::vector<std::string_view> GetTableEntries() const;
    
private:
    std::vector<std::string> tableEntries;
};
