#pragma once

#include "SerialCore.hpp"

// ----------------------------------------------------------------------------------------------------
// Main interface for explicit Store and Load operations
namespace SerialInterface
{
    using namespace SerialCore;
    
    // Single-item Store
    template < typename DataType, typename ArchiveType >
    inline void SerialStore( ArchiveType& rArchiveDest, const DataType& dataSource )
    {
        SerialImpl( SerialOp::Store, rArchiveDest, dataSource );
    }
    
    // Single-item Load, return-by-value
    template < typename DataType, typename ArchiveType >
	[[nodiscard]]
    inline DataType SerialLoad( const ArchiveType& rArchiveSource )
    {
        DataType result{};
        SerialImpl( SerialOp::Load, rArchiveSource, result );
        return result;
    }
    
    // Single-item Load, return-by-pointer
    template < typename DataType, typename ArchiveType >
    inline void SerialLoad( ArchiveType& rArchiveSource, DataType* pDataDest )
    {
        SerialImpl( SerialOp::Load, rArchiveSource, *pDataDest );
    }
    
    // Array Store
    template < typename DataType, typename ArchiveType >
    inline void SerialStore( ArchiveType& rArchiveDest, const DataType* pDataSource, std::size_t count )
    {
        SerialImpl( SerialOp::Store, rArchiveDest, pDataSource, count );
    }
    
    // Array Load
    template < typename DataType, typename ArchiveType >
    inline void SerialLoad( const ArchiveType& rArchiveSource, DataType* pDataDest, std::size_t count )
    {
        SerialImpl( SerialOp::Load, rArchiveSource, pDataDest, count );
    }
}
