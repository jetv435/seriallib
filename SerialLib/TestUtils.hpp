#pragma once

// ====================================================================================================
// TestUtils
// Changes:
//  Mar. 30, 2020
//      - PrintAssessment() marked inline to avoid duplicate-impl errors
//      - Removed <functional> include
//  Dec. 30, 2019
//      - When printing, tests passing all CHECKs will only print the success count for brevity
//  Oct. 8, 2019
//      - Non-NRS branch (much simpler, but lacks AutoTest utils)
//      - Added function-style syntax for TEST_END macro
//      - Section descriptions added (open for future elaboration)
//      - Assess() renamed to PrintAssessment()
//      - FailureInfo and TestReturn are sealed types
//      - TEST_START and TEST_END wrap the test interior into an anonymous function call
//      - TEST_END_PRINT added to enable the test to print its own results
// ====================================================================================================

#include <cstdint>
#include <cstdio>
#include <string>
#include <vector>

// Record of a CHECK failure
struct FailureInfo final
{
    std::string message;
    int line;
};

// Record of test's name, all its CHECK failures, and total number of CHECKs performed
struct TestReturn final
{
    std::string testName = "<no name provided>";
    std::vector<FailureInfo> failures;
    int numTests = 0;
};

// Prints test results
inline bool PrintAssessment( const TestReturn& results )
{
    printf( "\"%s\" - %s\n", results.testName.c_str(), results.failures.size() == 0 ? "SUCCESS" : "failed some checks" );
    
    if( results.failures.empty() )
    {
        printf( "  %llu CHECKs passed\n", static_cast<std::uint64_t>( results.numTests - results.failures.size() ) );
    }
    else
    {
        printf( "  %llu CHECKs failed out of %d (%llu passed)\n", static_cast<std::uint64_t>( results.failures.size() ), results.numTests, static_cast<std::uint64_t>( results.numTests - results.failures.size() ) );
    }
    
    for( const FailureInfo& failure : results.failures )
    {
        printf( "    line %d: \"%s\"\n", failure.line, failure.message.c_str() );
    }
    printf( "\n" );
    return results.failures.empty();
}

// Intended to bracket the entirety of a test's scope, initializing and returning test data to calling code
#define TEST_START(n) TestReturn _testRet; _testRet.testName = (n); [&_testRet]{
#define TEST_END() }(); return _testRet;
#define TEST_END_PRINT() PrintAssessment( _testRet ); TEST_END()

// Test discrete state and conditions during the running test
#define CHECK( cond )                                                      \
{                                                                          \
    if( !( cond ) )                                                        \
    {                                                                      \
        _testRet.failures.emplace_back( FailureInfo{ #cond, __LINE__ } );  \
    }                                                                      \
    ++_testRet.numTests;                                                   \
}
