#include "NameTests.hpp"

#include <cstdint>
#include <cstdio>

#include "TestUtils.hpp"
#include "SerialInterface.hpp"

#include "NameTable.hpp"
#include "NativeBufferArchive.hpp"
#include "NameTableSerialization.hpp"

static bool VectorContainsString( const std::vector<std::string_view>& strings, const std::string_view findme )
{
    for( const std::string_view& string : strings )
    {
        if( string == findme )
        {
            return true;
        }
    }
    return false;
}

namespace NameTests
{

    static TestReturn TestFindNameOnEmtpyTable()
    {
        TEST_START( "TestFindNameOnEmtpyTable" );
        
        NameTable testTable;
        const NameTable::NameID nullID = testTable.Find( "None" );
        CHECK( nullID == NameTable::NullNameID );
        
        TEST_END_PRINT();
    }
    
    static TestReturn TestFindOrAdd()
    {
        TEST_START( "TestFindOrAdd" );
        
        NameTable testTable;
        
        const NameTable::NameID newNameID = testTable.FindOrAdd( "Steering Wheel" );
        CHECK( newNameID != NameTable::NullNameID );
        
        // We should still get a null ID if we ask for one that doesn't exist in the table
        const NameTable::NameID nullNameID = testTable.Find( "None" );
        CHECK( nullNameID == NameTable::NullNameID );
        
        // If we ask for a name string's ID which exists in the table, its ID should always be consistent
        const NameTable::NameID foundNameID = testTable.Find( "Steering Wheel" );
        CHECK( foundNameID == newNameID );
        
        // Don't add a new entry with a new ID, give us a consistent ID every time
        const NameTable::NameID foundOrAddedNameID = testTable.FindOrAdd( "Steering Wheel" );
        CHECK( foundOrAddedNameID == newNameID );
        
        TEST_END_PRINT();
    }
    
    static TestReturn TestGetNameString()
    {
        TEST_START( "TestGetNameString" );
        
        NameTable testTable;
        
        constexpr char newNameString[] = "Hello";
        const NameTable::NameID newID = testTable.FindOrAdd( newNameString );
        
        // Valid ID
        const std::string_view fetchedStringView = testTable.GetNameString( newID );
        CHECK( fetchedStringView == newNameString );
        
        // Invalid ID
        constexpr std::string_view NullStringView{};
        const std::string_view nullStringView = testTable.GetNameString( NameTable::NullNameID );
        CHECK( nullStringView == NullStringView );
        
        TEST_END_PRINT();
    }
    
    static TestReturn TestGetEntries()
    {
        TEST_START( "TestGetEntries" );
        
        NameTable testTable;
        
        testTable.FindOrAdd( "Name A" );
        testTable.FindOrAdd( "Name B" );
        testTable.FindOrAdd( "Name C" );
        
        const std::vector<std::string_view> nameEntries = testTable.GetTableEntries();
        
        CHECK( VectorContainsString( nameEntries, "Name A" ) );
        CHECK( VectorContainsString( nameEntries, "Name B" ) );
        CHECK( VectorContainsString( nameEntries, "Name C" ) );

        TEST_END_PRINT();
    }
    
    static TestReturn TestSerializeNameTable()
    {
        TEST_START( "TestSerializeNameTable" );

        using namespace SerialInterface;

        NameTable::NameID nameofFloat32 = NameTable::NullNameID;
        NameTable::NameID nameofU16 = NameTable::NullNameID;
        NameTable::NameID nameofString = NameTable::NullNameID;
        NameTable::NameID nameofBool = NameTable::NullNameID;

        const NativeBufferArchive readArchive = [&]
        {
            NativeBufferArchive writeArchive;

            NameTable testTable;

            nameofFloat32 = testTable.FindOrAdd( "Float32" );
            nameofU16 = testTable.FindOrAdd( "U16" );
            nameofString = testTable.FindOrAdd( "String" );
            nameofBool = testTable.FindOrAdd( "Boolean" );

            SerialStore( writeArchive, testTable );

            writeArchive.SetSeek( 0 );
            return writeArchive;
        }();

        {
            CHECK( readArchive.GetSeek() == 0 );

            const NameTable testTable = SerialLoad<NameTable>( readArchive );

            CHECK( testTable.Find( "Boolean" ) == nameofBool );
            CHECK( testTable.Find( "String" ) == nameofString );
            CHECK( testTable.Find( "None" ) == NameTable::NullNameID );
            CHECK( testTable.Find( "U16" ) == nameofU16 );
            CHECK( testTable.Find( "Float32" ) == nameofFloat32 );
        }

        TEST_END_PRINT();
    }

#if !defined( TEST_AGGREGATE )
#define TEST_AGGREGATE( out_total_tests, out_total_checks, out_tests_passing, test_function ) { TestReturn reportTmp = test_function(); ++out_total_tests; out_total_checks += reportTmp.numTests; out_tests_passing += ( ( reportTmp.failures.size() == 0 ) ? 1 : 0 ); }
#else
#error TEST_AGGREGATE is defined elsewhere
#endif
    
    void Run()
    {
        printf( "\n----------------------------------------------------------------------------------------------------\n" );
        printf( "[NameTests]\n\n" );
        
        std::uint32_t numTotalTests = 0;
        std::uint32_t numTotalChecks = 0;
        std::uint32_t numTestsPassing = 0;
        
        TEST_AGGREGATE( numTotalTests, numTotalChecks, numTestsPassing, TestFindNameOnEmtpyTable );
        TEST_AGGREGATE( numTotalTests, numTotalChecks, numTestsPassing, TestFindOrAdd );
        TEST_AGGREGATE( numTotalTests, numTotalChecks, numTestsPassing, TestGetNameString );
        TEST_AGGREGATE( numTotalTests, numTotalChecks, numTestsPassing, TestGetEntries );
        TEST_AGGREGATE( numTotalTests, numTotalChecks, numTestsPassing, TestSerializeNameTable );

        printf( "Tests: %u of %u passing\n", numTestsPassing, numTotalTests );
        printf( "Total CHECKs: %u\n\n", numTotalChecks );
    }
    
#undef TEST_AGGREGATE

}
