#include "NameTests.hpp"
#include "SerialTests.hpp"

int main()
{
    SerialTests::Run();
    NameTests::Run();

    return 0;
}
