#include "NativeBufferArchive.hpp"

#include <cassert>

#include "MemUtils.hpp"

NativeBufferArchive::NativeBufferArchive()
    : storage()
    , seek( 0 )
{
}

NativeBufferArchive::NativeBufferArchive( std::size_t initialSize )
    : storage( initialSize )
    , seek( 0 )
{
}

NativeBufferArchive::NativeBufferArchive( const NativeBufferArchive& other )
    : storage( other.storage )
    , seek( 0 )
{
}

NativeBufferArchive::NativeBufferArchive( NativeBufferArchive&& other ) noexcept
    : storage( std::move( other.storage ) )
    , seek( 0 )
{
}

void NativeBufferArchive::WriteImpl( const std::byte* pSourceData, std::size_t count )
{
    assert( pSourceData );
    assert( count > 0 );
    
    const std::size_t spareBytes = storage.size() - seek;
    if( spareBytes < count  )
    {
        const std::size_t newBytesNeeded = count - spareBytes;
        storage.resize( storage.size() + newBytesNeeded );
    }
    
    MemUtils::Memcpy( storage.data() + seek, pSourceData, count );
    seek += count;
}

void NativeBufferArchive::ReadImpl( std::byte* pDestData, std::size_t count ) const
{
    assert( pDestData );
    assert( count > 0 );
    
    const std::size_t spareBytes = storage.size() - seek;
    assert( spareBytes >= count );
    
    MemUtils::Memcpy( pDestData, storage.data() + seek, count );
    seek += count;
}

std::byte* NativeBufferArchive::GetRawStorage()
{
    return storage.data();
}

const std::byte* NativeBufferArchive::GetRawStorage() const
{
    return storage.data();
}

std::size_t NativeBufferArchive::GetSeek() const noexcept
{
    return seek;
}

void NativeBufferArchive::SetSeek( std::size_t newSeek ) const noexcept
{
    assert( newSeek <= storage.size() );
    seek = newSeek;
}

void NativeBufferArchive::SeekToEnd() const noexcept
{
    seek = storage.size();
}

std::size_t NativeBufferArchive::GetSize() const noexcept
{
    return storage.size();
}

void NativeBufferArchive::ClearAndReset()
{
    storage.clear();
    seek = 0;
}

void NativeBufferArchive::Resize( std::size_t newSize )
{
    storage.resize( newSize );
    seek = 0;
}
