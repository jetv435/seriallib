#pragma once

#include <type_traits>
#include <vector>

#include "ArchiveTraits.hpp"
#include "SerialTraits.hpp"

class NativeBufferArchive final
{
public:
    // Default construction of empty storage
    NativeBufferArchive();
    NativeBufferArchive( std::size_t initialSize );
    ~NativeBufferArchive() = default;
    
    // ...
    NativeBufferArchive( const NativeBufferArchive& other );
    NativeBufferArchive& operator=( const NativeBufferArchive& ) = delete;
    
    // ...
    NativeBufferArchive( NativeBufferArchive&& other ) noexcept;
    NativeBufferArchive& operator=( NativeBufferArchive&& ) = delete;
    
    // Write an instance or an array of instances by-pointer
    template < typename DataT >
    inline void Write( const DataT* pSourceData, std::size_t count )
    {
        static_assert( std::is_trivially_copyable_v<DataT>, "DataT instances must be trivially copyable." );
        const std::size_t fullWriteSize = sizeof( DataT ) * count;
        WriteImpl( reinterpret_cast<const std::byte*>( pSourceData ), fullWriteSize );
    }
    
    // Write a single instance (accepts rvalue and literal without needing named temporaries)
    template < typename DataT >
    inline void Write( const DataT rSourceData )
    {
        Write( &rSourceData, 1 );
    }
    
    // Read an instance or an array of instances by-pointer
    template < typename DataT >
    inline void Read( DataT* pDestData, std::size_t count ) const
    {
        static_assert( std::is_trivially_copyable_v<DataT>, "DataT instances must be trivially copyable." );
        const std::size_t fullReadSize = sizeof( DataT ) * count;
        ReadImpl( reinterpret_cast<std::byte*>(pDestData), fullReadSize );
    }
    
    // Read a single instance and return it by-value; NVRO may and should elide the copy
    template < typename DataT >
	[[nodiscard]]
    inline const DataT Read() const
    {
        DataT result;
        Read( &result, 1 );
        return result;
    }
    
    std::size_t GetSeek() const noexcept;
    void SetSeek( std::size_t newSeek ) const noexcept;
    void SeekToEnd() const noexcept;
    std::size_t GetSize() const noexcept;
    void ClearAndReset();
    void Resize( std::size_t newSize );
    
    std::byte* GetRawStorage();
    const std::byte* GetRawStorage() const;
    
private:
    void WriteImpl( const std::byte* pSourceData, std::size_t count );
    void ReadImpl( std::byte* pDestData, std::size_t count ) const;
    
    std::vector<std::byte> storage;
    mutable std::size_t seek;
};

// Override default ArchiveTraits
namespace ArchiveTraits
{
    template <>
    struct IsArchive_Trait<NativeBufferArchive> final
    {
        static constexpr bool value = true;
    };
    
    template < typename DataType >
    struct TriviallySerializesType_Trait<NativeBufferArchive, DataType> final
    {
        static constexpr bool value = std::is_trivially_copyable_v<DataType>;
    };
    
    template <>
    struct SafeToSerializeFromNativeSystem_Trait<NativeBufferArchive> final
    {
        static constexpr bool value = true;
    };
}
