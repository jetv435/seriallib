#pragma once

#include <array>
#include <list>
#include <string>
#include <vector>

#include "SerialInterface.hpp"

// ----------------------------------------------------------------------------------------------------
// STL Array

template < typename ArchiveType, typename ArrayItemType, std::size_t ArraySize >
void SerialImpl( SerialCore::SerialOp::StoreT, ArchiveType& rArchiveDest, const std::array<ArrayItemType, ArraySize>& rArraySource )
{
    SerialInterface::SerialStore( rArchiveDest, rArraySource.data(), ArraySize );
}

template < typename ArchiveType, typename ArrayItemType, std::size_t ArraySize >
void SerialImpl( SerialCore::SerialOp::LoadT, const ArchiveType& rArchiveSource, std::array<ArrayItemType, ArraySize>& rArrayDest )
{
    SerialInterface::SerialLoad( rArchiveSource, rArrayDest.data(), ArraySize );
}

// ----------------------------------------------------------------------------------------------------
// STL Vector

template < typename ArchiveType, typename VectorItemType >
void SerialImpl( SerialCore::SerialOp::StoreT, ArchiveType& rArchiveDest, const std::vector<VectorItemType>& rVectorSource )
{
    static_assert( sizeof( std::uint64_t ) >= sizeof( typename std::vector<VectorItemType>::size_type ), "std::vector::size_type should fit within the serial representation without truncation" );
    
    using namespace SerialInterface;

    SerialStore<std::uint64_t>( rArchiveDest, rVectorSource.size() );
    if( !rVectorSource.empty() )
    {
        SerialStore( rArchiveDest, rVectorSource.data(), rVectorSource.size() );
    }
}

template < typename ArchiveType, typename VectorItemType >
void SerialImpl( SerialCore::SerialOp::LoadT, const ArchiveType& rArchiveSource, std::vector<VectorItemType>& rVectorDest )
{
    static_assert( sizeof( std::uint64_t ) >= sizeof( typename std::vector<VectorItemType>::size_type ), "std::vector::size_type should fit within the serial representation without truncation" );
    
    using namespace SerialInterface;
    
    assert( rVectorDest.empty() );
    
    const std::uint64_t vectSize = SerialLoad<std::uint64_t>( rArchiveSource );
    if( vectSize > 0 )
    {
		assert( vectSize <= std::numeric_limits<std::size_t>::max() );
        rVectorDest.resize( static_cast<std::size_t>( vectSize ) );
        SerialLoad( rArchiveSource, rVectorDest.data(), rVectorDest.size() );
    }
}

// ----------------------------------------------------------------------------------------------------
// STL List

template < typename ArchiveType, typename ListItemType >
void SerialImpl( SerialCore::SerialOp::StoreT, ArchiveType& rArchiveDest, const std::list<ListItemType>& rListSource )
{
    static_assert( sizeof( std::uint64_t ) >= sizeof( typename std::list<ListItemType>::size_type ), "std::list::size_type should fit within the serial representation without truncation" );
    
    using namespace SerialInterface;
    
    SerialStore<std::uint64_t>( rArchiveDest, rListSource.size() );
    for( const ListItemType& rItem : rListSource )
    {
        SerialStore( rArchiveDest, rItem );
    }
}

template < typename ArchiveType, typename ListItemType >
void SerialImpl( SerialCore::SerialOp::LoadT, const ArchiveType& rArchiveSource, std::list<ListItemType>& rListDest )
{
    static_assert( sizeof( std::uint64_t ) >= sizeof( typename std::list<ListItemType>::size_type ), "std::list::size_type should fit within the serial representation without truncation" );
    
    using namespace SerialInterface;
    
    assert( rListDest.empty() );
    
    const std::uint64_t listSize = SerialLoad<std::uint64_t>( rArchiveSource );
    assert( listSize <= std::numeric_limits<std::size_t>::max() );
    for( std::uint64_t index = 0; index < listSize ; ++index )
    {
        rListDest.push_back( SerialLoad<ListItemType>( rArchiveSource ) );
    }
}

// ----------------------------------------------------------------------------------------------------
// STL String View (Store only)

namespace SerialTraits
{
    template <>
    struct TypeMayBeTriviallySerialized_Trait<std::string_view> final
    {
        // std::string_view is trivially copyable so manually opt-out for trivial serialization
        static constexpr bool value = false;
    };
}

template < typename ArchiveType >
void SerialImpl( SerialCore::SerialOp::StoreT, ArchiveType& rArchiveDest, std::string_view stringSource )
{
    static_assert( sizeof( std::uint64_t ) >= sizeof( typename std::string_view::size_type ), "std::string::size_type should fit within the serial representation without truncation" );
    
    using namespace SerialInterface;
    
    SerialStore<std::uint64_t>( rArchiveDest, stringSource.size() );
    SerialStore( rArchiveDest, reinterpret_cast<const std::byte*>( stringSource.data() ), stringSource.size() );
}

// ----------------------------------------------------------------------------------------------------
// STL String

template < typename ArchiveType >
void SerialImpl( SerialCore::SerialOp::StoreT, ArchiveType& rArchiveDest, const std::string& rStringSource )
{
    static_assert( std::is_same_v< std::string_view::size_type, std::string::size_type >, "std::string_view and std::string must use the same size_type" );
    SerialInterface::SerialStore<std::string_view>( rArchiveDest, static_cast<std::string_view>( rStringSource ) );
}

template < typename ArchiveType >
void SerialImpl( SerialCore::SerialOp::LoadT, const ArchiveType& rArchiveSource, std::string& rStringDest )
{
    static_assert( sizeof( std::uint64_t ) >= sizeof( typename std::string::size_type ), "std::string::size_type should fit within the serial representation without truncation" );
    
    using namespace SerialInterface;
    
    assert( rStringDest.empty() );
    
    const std::uint64_t strDataSize = SerialLoad<std::uint64_t>( rArchiveSource );
    if( strDataSize > 0)
    {
		assert( strDataSize <= std::numeric_limits<std::size_t>::max() );
        rStringDest.resize( static_cast<std::size_t>( strDataSize ) );
        SerialLoad( rArchiveSource, reinterpret_cast<std::byte*>( rStringDest.data() ), rStringDest.size() );
    }
}
