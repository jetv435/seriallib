#include <type_traits>

#include "TypeList.hpp"

// TypeList Size
using TestTypesA = TypeList<int, float, bool>;
using TestTypesEmpty = TypeList<>;
static_assert( TypeListFuncs::Size<TestTypesA> == 3, "Wrong!" );
static_assert( TypeListFuncs::Size<TestTypesEmpty> == 0, "Wrong!" );

// TypeList Front
using TestTypesSingle = TypeList<short>;
static_assert( std::is_same_v<TypeListFuncs::Front<TestTypesA>, int>, "Wrong!" );
static_assert( std::is_same_v<TypeListFuncs::Front<TestTypesSingle>, short>, "Wrong!" );

// TypeList PopFront
using TestTypesB = TypeListFuncs::PopFront<TestTypesA>;
using TestTypesC = TypeListFuncs::PopFront<TestTypesB>;
using TestTypesD = TypeListFuncs::PopFront<TestTypesC>;
static_assert( std::is_same_v<TypeListFuncs::Front<TestTypesB>, float>, "Wrong!" );
static_assert( std::is_same_v<TypeListFuncs::Front<TestTypesC>, bool>, "Wrong!" );
static_assert( TypeListFuncs::Size<TestTypesD> == 0, "Wrong!" );

// TypeList CompareOrdered
static_assert( TypeListFuncs::CompareOrdered<TestTypesA, TypeList<int, float, bool>>, "Wrong!" );
static_assert( TypeListFuncs::CompareOrdered<TestTypesSingle, TypeList<short>>, "Wrong!" );
static_assert( !TypeListFuncs::CompareOrdered<TestTypesC, TypeList<int, short, char>>, "Wrong!" );

// TypeList Satisfies
struct TestStruct final
{
    int a;
    bool b;
};

using TestTypesE = TypeList<float, TestStruct>;
static_assert( TypeListFuncs::Satisfies<TestTypesA, std::is_fundamental>, "Wrong!" );
static_assert( !TypeListFuncs::Satisfies<TestTypesE, std::is_fundamental>, "Wrong!" );
static_assert( TypeListFuncs::Satisfies<TestTypesA, std::is_fundamental, std::is_trivially_copyable, std::is_scalar>, "Wrong!" );
static_assert( TypeListFuncs::Satisfies<TestTypesSingle, std::is_fundamental, std::is_trivially_copyable>, "Wrong!" );

// TypeList Contains
static_assert( TypeListFuncs::Contains<TestTypesA, float>, "Wrong!" );
static_assert( !TypeListFuncs::Contains<TestTypesE, short>, "Wrong!" );
static_assert( !TypeListFuncs::Contains<TestTypesEmpty, int>, "Wrong!" );

// TypeList Unique
using TestTypesF = TypeList<float, int, bool, short, int>;
static_assert( TypeListFuncs::Unique<TestTypesA>, "Wrong!" );
static_assert( !TypeListFuncs::Unique<TestTypesF>, "Wrong!" );

// TypeList Append
using TestTypesG = TypeListFuncs::Append<TestTypesF, TestTypesA>;
using TestTypesH = TypeListFuncs::Append<TestTypesSingle, TestTypesA, TestTypesE>;
static_assert( TypeListFuncs::CompareOrdered<TestTypesG, TypeList<float, int, bool, short, int, int, float, bool>>, "Wrong!" );
static_assert( TypeListFuncs::CompareOrdered<TestTypesH, TypeList<short, int, float, bool, float, TestStruct>>, "Wrong!" );

// TypeList ConvertToTypeList
template < typename TypeA, typename TypeB, typename TypeC >
struct SomeTemplatizedType;

using TestTypesI = TypeListFuncs::ConvertToTypeList<SomeTemplatizedType<bool, int, short>>;
static_assert( TypeListFuncs::CompareOrdered<TestTypesI, TypeList<bool, int, short>>, "Wrong!" );
