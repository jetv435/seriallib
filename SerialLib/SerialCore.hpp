#pragma once

#include <cassert>

#include "ArchiveTraits.hpp"
#include "SerialTraits.hpp"

namespace SerialCore
{
    // ----------------------------------------------------------------------------------------------------
    // Type-exclusive Store and Load constants for function/template overloading
    
    namespace SerialOp
    {
        enum StoreT { Store };
        enum LoadT { Load };
    }
    
    // ----------------------------------------------------------------------------------------------------
    // Trait utilities for checking SerialOp at compile-time
    
    namespace SerialCoreImpl
    {
        template < typename SerialOpT >
        struct SerialOpTypeValidator final
        {
            static_assert( std::is_same_v<SerialOpT, SerialOp::StoreT> || std::is_same_v<SerialOpT, SerialOp::LoadT>, "Invalid SerialOp" );
            static constexpr bool value = true;
        };
    }
    
    template < typename SerialOpT >
    constexpr bool IsStoring = std::is_same_v<SerialOpT, SerialOp::StoreT> && SerialCoreImpl::SerialOpTypeValidator<SerialOpT>::value;
    
    template < typename SerialOpT >
    constexpr bool IsLoading = std::is_same_v<SerialOpT, SerialOp::LoadT> && SerialCoreImpl::SerialOpTypeValidator<SerialOpT>::value;

    // ----------------------------------------------------------------------------------------------------
    // Deduces whether a type can be trivially serialized based on the ArchiveType and DataType
    
    namespace SerialCoreImpl
    {
        template < typename ArchiveType, typename DataType >
        constexpr bool TrivialSerializeCondition
            = SerialTraits::TypeMayBeTriviallySerialized<DataType>              // DataTypes may opt-out of being trivially serialized
            && ArchiveTraits::TriviallySerializesType<ArchiveType, DataType>;   // ArchiveType can safely trivially serialize DataType
    
        template < typename ArchiveType, typename DataType >
        using TrivialSerializeEnableIf = std::enable_if_t<SerialCoreImpl::TrivialSerializeCondition<ArchiveType, DataType>, int>;
    }

    // ----------------------------------------------------------------------------------------------------
    // Core SerialImpl() implementations which call the archive's Write() or Read() with provided data

    template < typename DataType, typename ArchiveType, SerialCoreImpl::TrivialSerializeEnableIf<ArchiveType, DataType> = 0 >
    void SerialImpl( SerialOp::StoreT, ArchiveType& rArchiveDest, const DataType dataSource )
    {
        static_assert( ArchiveTraits::IsArchive<ArchiveType>, "Given ArchiveType hasn't overrided 'IsArchive' type-trait to return 'true'" );
        static_assert( ArchiveTraits::SafeToSerializeFromNativeSystem<ArchiveType>, "Given ArchiveType can't serialize data from the native platform" );
        rArchiveDest.template Write<DataType>( dataSource );
    }

    template < typename DataType, typename ArchiveType, SerialCoreImpl::TrivialSerializeEnableIf<ArchiveType, DataType> = 0 >
    void SerialImpl( SerialOp::LoadT, const ArchiveType& rArchiveSource, DataType& rDataDest )
    {
        static_assert( ArchiveTraits::IsArchive<ArchiveType>, "Given ArchiveType hasn't overrided 'IsArchive' type-trait to return 'true'" );
        static_assert( ArchiveTraits::SafeToSerializeFromNativeSystem<ArchiveType>, "Given ArchiveType can't serialize data from the native platform" );
        rDataDest = rArchiveSource.template Read<DataType>();
    }

    template < typename DataType, typename ArchiveType, SerialCoreImpl::TrivialSerializeEnableIf<ArchiveType, DataType> = 0 >
    void SerialImpl( SerialOp::StoreT, ArchiveType& rArchiveDest, const DataType* pDataSource, std::size_t count )
    {
        static_assert( ArchiveTraits::IsArchive<ArchiveType>, "Given ArchiveType hasn't overrided 'IsArchive' type-trait to return 'true'" );
        static_assert( ArchiveTraits::SafeToSerializeFromNativeSystem<ArchiveType>, "Given ArchiveType can't serialize data from the native platform" );
        assert( pDataSource );
        rArchiveDest.template Write<DataType>( pDataSource, count );
    }

    template < typename DataType, typename ArchiveType, SerialCoreImpl::TrivialSerializeEnableIf<ArchiveType, DataType> = 0 >
    void SerialImpl( SerialOp::LoadT, const ArchiveType& rArchiveSource, DataType* pDataDest, std::size_t count )
    {
        static_assert( ArchiveTraits::IsArchive<ArchiveType>, "Given ArchiveType hasn't overrided 'IsArchive' type-trait to return 'true'" );
        static_assert( ArchiveTraits::SafeToSerializeFromNativeSystem<ArchiveType>, "Given ArchiveType can't serialize data from the native platform" );
        assert( pDataDest );
        rArchiveSource.template Read<DataType>( pDataDest, count );
    }

    // ----------------------------------------------------------------------------------------------------
    // Serial operations on booleans
    
    namespace SerialCoreImpl
    {
        constexpr std::uint8_t SerialUInt8False = 0x00;
        constexpr std::uint8_t SerialUInt8True = 0x01;
    }
    
    template < typename ArchiveType >
    inline void SerialImpl( SerialOp::StoreT, ArchiveType& rArchiveDest, bool boolSource )
    {
        using namespace SerialCoreImpl;
        const std::uint8_t asUInt8 = boolSource ? SerialUInt8True : SerialUInt8False;
        SerialImpl( SerialOp::Store, rArchiveDest, asUInt8 );
    }
    
    template < typename ArchiveType >
    inline void SerialImpl( SerialOp::LoadT, const ArchiveType& rArchiveSource, bool& rBoolDest )
    {
        using namespace SerialCoreImpl;
        std::uint8_t asUInt8;
        SerialImpl( SerialOp::Load, rArchiveSource, asUInt8 );
        assert( asUInt8 == SerialUInt8True || asUInt8 == SerialUInt8False );
        rBoolDest = ( asUInt8 == SerialUInt8True ) ? true : false;
    }

    // ----------------------------------------------------------------------------------------------------
    // Serial operations on pointers
    
    template < typename DataType, typename ArchiveType >
    void SerialImpl( SerialOp::StoreT, ArchiveType& rArchiveDest, const DataType* const& pDataSourcePtr )
    {
        if( pDataSourcePtr )
        {
            SerialImpl( SerialOp::Store, rArchiveDest, true );
            SerialImpl( SerialOp::Store, rArchiveDest, *pDataSourcePtr );
        }
        else
        {
            SerialImpl( SerialOp::Store, rArchiveDest, false );
        }
    }

    template < typename DataType, typename ArchiveType >
    void SerialImpl( SerialOp::LoadT, const ArchiveType& rArchiveSource, DataType*& rDataDestPtr )
    {
        assert( !rDataDestPtr );
        
        bool bNotNull = false;
        SerialImpl( SerialOp::Load, rArchiveSource, bNotNull );
        if( bNotNull && !rDataDestPtr )
        {
            rDataDestPtr = new DataType();
            SerialImpl( SerialOp::Load, rArchiveSource, *rDataDestPtr );
        }
        else
        {
            rDataDestPtr = nullptr;
        }
    }

    // ----------------------------------------------------------------------------------------------------
    // Provides piecemeal serialization on each array item when the type can't be trivially serialized
    //  Important: This is declared below the bool and pointer specializations in case it needs to call either

    template < typename DataType, typename ArchiveType, typename SerialOpT >
    inline void SerialImpl( SerialOpT, ArchiveType& rArchive, DataType* pData, std::size_t count )
    {
        assert( pData );
        for ( std::size_t index = 0; index < count; ++index )
        {
            SerialImpl( SerialOpT{}, rArchive, pData[index] );
        }
    }

    // ----------------------------------------------------------------------------------------------------
    // Serial operations on raw C arrays (for now, define two separate rather than ambidextrous to avoid
    //  template ambiguity vs. the core SerialImpl when DataType is trivially serializable)

    template < typename DataType, std::size_t ItemCount, typename ArchiveType >
    inline void SerialImpl( SerialOp::StoreT, ArchiveType& rArchiveDest, const DataType( &rArraySource )[ItemCount] )
    {
        SerialImpl( SerialOp::Store, rArchiveDest, &rArraySource[0], ItemCount );
    }

    template < typename DataType, std::size_t ItemCount, typename ArchiveType >
    inline void SerialImpl( SerialOp::LoadT, const ArchiveType& rArchiveSource, DataType( &rArrayDest )[ItemCount] )
    {
        SerialImpl( SerialOp::Load, rArchiveSource, &rArrayDest[0], ItemCount );
    }

    // ----------------------------------------------------------------------------------------------------
    // Wraps the data argument type passed into SerialImpl() when supporting ambidextrous implementations
    
    namespace SerialCoreImpl
    {
        template < typename... >
        struct AmbidextrousArgImpl;
        
        template < typename DataType >
        struct AmbidextrousArgImpl<SerialOp::StoreT, DataType> final
        {
        private:
            using implDecayedT = std::decay_t<DataType>;
            using implConstDecayedT = std::add_const_t<implDecayedT>;
            static constexpr bool implWasReference = std::is_lvalue_reference_v<DataType>;
        public:
            using type = std::conditional_t<implWasReference, std::add_lvalue_reference_t<implConstDecayedT>, implConstDecayedT>;
        };
        
        template < typename DataType >
        struct AmbidextrousArgImpl<SerialOp::LoadT, DataType> final
        {
            static_assert( !std::is_const_v<std::remove_reference_t<DataType>>, "Given type must be non-const qualified" );
            using type = std::add_lvalue_reference_t<DataType>;
        };
    }

    template < typename SerialOpT, typename DataType >
    using AmbidextrousArg = typename SerialCoreImpl::AmbidextrousArgImpl<SerialOpT, DataType>::type;
}
