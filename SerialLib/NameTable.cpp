#include "NameTable.hpp"

NameTable::NameID NameTable::Find( std::string_view nameString ) const
{
    for( NameID currID = static_cast<NameID>( 0 ); currID < static_cast<NameID>( tableEntries.size() ); ++currID )
    {
        if( tableEntries[currID] == nameString )
        {
            return currID;
        }
    }
    return NullNameID;
}

NameTable::NameID NameTable::FindOrAdd( std::string_view nameString )
{
    NameID result = Find( nameString );
    
    if( result == NullNameID )
    {
        tableEntries.emplace_back( nameString );
        result = static_cast<NameID>( tableEntries.size() - 1 );
    }
    
    return result;
}

const std::string_view NameTable::GetNameString( NameID ID ) const
{
    if( ID <= NullNameID )
    {
        return std::string_view{};
    }
    else
    {
        return tableEntries[ID];
    }
}

const std::vector<std::string_view> NameTable::GetTableEntries() const
{
    std::vector<std::string_view> results;
    
    results.reserve( tableEntries.size() );
    for( const std::string& entry : tableEntries )
    {
        results.emplace_back( static_cast<std::string_view>( entry ) );
    }
    
    return results;
}
