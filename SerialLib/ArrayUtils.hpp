#pragma once

#include <cstddef>

namespace ArrayUtils
{
	template < typename T, std::size_t ItemCount >
    [[nodiscard]]
	constexpr std::size_t CountOf( const T( & )[ItemCount] ) noexcept
	{
		return ItemCount;
	}

	template < typename T, std::size_t ItemCount >
    [[nodiscard]]
	constexpr std::size_t SizeOf( const T( &arr )[ItemCount] ) noexcept
	{
		return sizeof( decltype( arr ) );
	}
}
