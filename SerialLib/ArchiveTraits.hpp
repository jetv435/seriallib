#pragma once

#include <type_traits>

namespace ArchiveTraits
{
    // ---------------------------------------------------------------------------------------------------------
    // Each type intended to behave as an Archive must override 'IsArchiveTrait' to return 'true'
    
    template < typename T >
    struct IsArchive_Trait final
    {
        static constexpr bool value = false;
    };
    
    template < typename T >
    constexpr bool IsArchive = IsArchive_Trait<T>::value;
    
    // ---------------------------------------------------------------------------------------------------------
    // Archives must explicitly override the criteria for which types it can or can't trivially serialize
    
    template < typename ArchiveType, typename DataType >
    struct TriviallySerializesType_Trait final
    {
        static constexpr bool value = false;
    };

    template < typename ArchiveType, typename DataType >
    constexpr bool TriviallySerializesType = TriviallySerializesType_Trait<ArchiveType, DataType>::value;
    
    // ---------------------------------------------------------------------------------------------------------
    // Archives must explicitly override whether or not its interface is safe to call from the native system
    
    template < typename ArchiveType >
    struct SafeToSerializeFromNativeSystem_Trait final
    {
        static constexpr bool value = false;
    };
    
    template < typename ArchiveType >
    constexpr bool SafeToSerializeFromNativeSystem = SafeToSerializeFromNativeSystem_Trait<ArchiveType>::value;
}
