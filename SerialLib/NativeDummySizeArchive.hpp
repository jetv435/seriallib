#pragma once

#include "ArchiveTraits.hpp"

class NativeDummySizeArchive final
{
public:
    NativeDummySizeArchive() noexcept = default;
    ~NativeDummySizeArchive() noexcept = default;
    NativeDummySizeArchive( const NativeDummySizeArchive& ) noexcept = default;
    NativeDummySizeArchive& operator=( const NativeDummySizeArchive& ) noexcept = default;
    
    // Write an instance or an array of instances by-pointer
    template < typename DataT >
    inline void Write( const DataT* pSourceData, std::size_t count ) noexcept
    {
		(void)pSourceData;
        static_assert( std::is_trivially_copyable_v<DataT>, "DataT instances must be trivially copyable." );
        const std::size_t fullWriteSize = sizeof( DataT ) * count;
        byteCount += fullWriteSize;
    }
    
    // Write a single instance (accepts rvalue and literal without needing named temporaries)
    template < typename DataT >
    inline void Write( const DataT rSourceData ) noexcept
    {
        Write( &rSourceData, 1 );
    }
    
    inline std::size_t GetSize() const noexcept
    {
        return byteCount;
    }
    
    inline void ResetCount() noexcept
    {
        byteCount = 0;
    }
    
private:
    std::size_t byteCount = 0;
};

// Override default ArchiveTraits
namespace ArchiveTraits
{
    template <>
    struct IsArchive_Trait<NativeDummySizeArchive> final
    {
        static constexpr bool value = true;
    };
    
    template < typename DataType >
    struct TriviallySerializesType_Trait<NativeDummySizeArchive, DataType> final
    {
        static constexpr bool value = std::is_trivially_copyable_v<DataType>;
    };
    
    template <>
    struct SafeToSerializeFromNativeSystem_Trait<NativeDummySizeArchive> final
    {
        static constexpr bool value = true;
    };
}
