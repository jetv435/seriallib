#pragma once

#include <cstddef>

#include "TypeList.hpp"

namespace SerialTraits
{
    // ----------------------------------------------------------------------------------------------------
    // Trivially serializable types + types which implicitly need non-trivial serialization
    
    template < typename DataType >
    struct TypeMayBeTriviallySerialized_Trait final
    {
        static constexpr bool value = std::is_trivially_copyable_v<DataType>;
    };
    
    template < typename DataType >
    struct TypeMayBeTriviallySerialized_Trait<DataType*> final
    {
        static constexpr bool value = false;
    };
    
    template <>
    struct TypeMayBeTriviallySerialized_Trait<bool> final
    {
        static constexpr bool value = false;
    };
    
    template < typename DataType >
    constexpr bool TypeMayBeTriviallySerialized = TypeMayBeTriviallySerialized_Trait<DataType>::value;
    
    // ----------------------------------------------------------------------------------------------------
    // Types compatible with 'strict serialization', i.e. whose byte representation should be deterministic
    //  accross platforms; useful when trivial serialization isn't robust/correct on non-strict types
    
    using StrictSerialFundamentalTypes = TypeList
    < std::uint64_t
    , std::uint32_t
    , std::uint16_t
    , std::uint8_t
    , std::int64_t
    , std::int32_t
    , std::int16_t
    , std::int8_t
    , char32_t
    , char16_t
    , signed char
    , unsigned char
    , float
    , double >;
    
    static_assert( TypeListFuncs::Satisfies<StrictSerialFundamentalTypes, std::is_fundamental>, "Types in list must be fundamental" );
    static_assert( sizeof( float ) == 4, "sizeof(float) expected to be 4 bytes" );
    static_assert( sizeof( double ) == 8, "sizeof(double) expected to be 8 bytes" );
    
    using StrictSerialTypes = TypeListFuncs::Append<StrictSerialFundamentalTypes, TypeList<std::byte>>;

    template < typename DataType >
    struct IsStrictSerialType_Trait final
    {
        static constexpr bool value = TypeListFuncs::Contains<StrictSerialTypes, DataType>;
    };
    
    template < typename DataType >
    constexpr bool IsStrictSerialType = IsStrictSerialType_Trait<DataType>::value;
}
