#include "NativeFileArchive.hpp"

#include <cassert>
#include <cstdio>

#include "MemUtils.hpp"
#include "Platform.hpp"

const NativeFileArchive::HandleAlias NativeFileArchive::DefaultNullFileHandle = 0;

NativeFileArchive::NativeFileArchive() noexcept
    : handle( DefaultNullFileHandle )
{
}

NativeFileArchive::~NativeFileArchive()
{
	if ( IsOpen() )
	{
		Close();
	}
}

bool NativeFileArchive::Open( NativeFileArchive& fileArchive, const char* filename, ExistenceCondition existCond )
{
    static_assert( sizeof( FILE* ) <= sizeof( HandleAlias ), "File HandleAlias doesn't match size used in implementation" );

    if( !fileArchive.IsOpen() )
    {
        const char* const modeSpec = [existCond]() noexcept -> const char*
        {
            switch ( existCond )
            {
                case ExistenceCondition::MustExistOnOpen:
                {
                    return "rb+";
                } break;
                case ExistenceCondition::CreateOrOverwriteOnOpen:
                {
                    return "wb+";
                } break;
                default:
                {
                    return nullptr;
                }
            }
        }();
        assert( modeSpec );
        
        FILE* const fileHandle = [filename, modeSpec]()
        {
#if PLATFORM_IS_WINDOWS()
            FILE* fileHandleReturn;
            fopen_s( &fileHandleReturn, filename, modeSpec );
            return fileHandleReturn;
#elif PLATFORM_IS_MACOSX()
            return std::fopen( filename, modeSpec );
#else
#error Unrecognized platform, can't determine correct cstd file open api to use
#endif
        }();
    
        MemUtils::Memcpy( fileArchive.handle, fileHandle );
        return fileArchive.IsOpen();
    }
    else
    {
        return false;
    }
}

void NativeFileArchive::Close()
{
    FILE* const fileHandle = MemUtils::MemReadAs<FILE*>( handle );
    int ret = std::fclose( fileHandle );
    (void)ret;
    assert( ret == 0 );
    handle = DefaultNullFileHandle;
}

bool NativeFileArchive::IsOpen() const noexcept
{
    return handle != DefaultNullFileHandle;
}

void NativeFileArchive::WriteImpl( const std::byte* pSourceData, std::size_t count )
{
    assert( pSourceData );
    assert( count > 0 );
    
    FILE* const fileHandle = MemUtils::MemReadAs<FILE*>( handle );
    std::size_t ret = std::fwrite( pSourceData, 1, count, fileHandle );
    (void)ret;
    assert( ret == count );
}

void NativeFileArchive::ReadImpl( std::byte* pDestData, std::size_t count ) const
{
    assert( pDestData );
    assert( count > 0 );
    
    FILE* const fileHandle = MemUtils::MemReadAs<FILE*>( handle );
    std::size_t ret = std::fread( pDestData, 1, count, fileHandle );
    (void)ret;
    assert( ret == count );
}

std::size_t NativeFileArchive::GetSeek() const
{
    FILE* const fileHandle = MemUtils::MemReadAs<FILE*>( handle );
    return static_cast<std::size_t>( std::ftell( fileHandle ) );
}

void NativeFileArchive::SetSeek( std::size_t newSeek ) const
{
    FILE* const fileHandle = MemUtils::MemReadAs<FILE*>( handle );
    int ret = std::fseek( fileHandle, static_cast<long>( newSeek ), 0 );
    (void)ret;
    assert( ret == 0 );
}

void NativeFileArchive::SeekToEnd() const
{
    FILE* const fileHandle = MemUtils::MemReadAs<FILE*>( handle );
    int ret = std::fseek( fileHandle, 0, SEEK_END );
    (void)ret;
    assert( ret == 0 );
}

std::size_t NativeFileArchive::GetSize() const
{
    const std::size_t bookmark = GetSeek();
    SeekToEnd();
    const std::size_t result = GetSeek();
    SetSeek( bookmark );
    return result;
}
